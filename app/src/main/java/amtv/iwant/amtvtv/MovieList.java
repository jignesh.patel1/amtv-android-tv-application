/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package amtv.iwant.amtvtv;

import java.util.ArrayList;
import java.util.List;

public final class MovieList {
    public static final String MOVIE_CATEGORY[] = {
            "Live TV Channels",
            "Blockbuster Movies",
            "Indonesian Movies",
            "Live TV Channels",
            "Blockbuster Movies",
            "Indonesian Movies"
    };

    private static List<Movie> list;
    private static long count = 0;

    public static List<Movie> getList() {
        if (list == null) {
            list = setupMovies();
        }
        return list;
    }

    public static List<Movie> setupMovies() {
        list = new ArrayList<>();
        String title[] = {
                "Zeitgeist 2010_ Year in Review",
                "Google Demo Slam_ 20ft Search",
                "Introducing Gmail Blue",
                "Introducing Google Fiber to the Pole",
                "Introducing Google Nose"
        };

        String description = "Fusce id nisi turpis. Praesent viverra bibendum semper. "
                + "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est "
                + "quis tellus. Sed mollis orci venenatis quam scelerisque accumsan. Curabitur a massa sit "
                + "amet mi accumsan mollis sed et magna. Vivamus sed aliquam risus. Nulla eget dolor in elit "
                + "facilisis mattis. Ut aliquet luctus lacus. Phasellus nec commodo erat. Praesent tempus id "
                + "lectus ac scelerisque. Maecenas pretium cursus lectus id volutpat.";
        String studio[] = {
                "Studio Zero", "Studio One", "Studio Two", "Studio Three", "Studio Four"
        };
        String videoUrl[] = {
                "http://103.214.185.98/NXoBGxyj/NXoBGxyj.m3u8",
                "http://edge.linknetott.swiftserve.com/live/BSgroup/amlst:homelivinghd/playlist.m3u8",
                "http://103.214.185.98/4t68X4Ay/4t68X4Ay.m3u8",
                "http://edge.linknetott.swiftserve.com/live/BSgroup/amlst:dangdutch/playlist.m3u8",
                "http://demo-thunderstorm.amagi.com/amagi_hls_data_amtvAAAAA-foodie/CDN/master.m3u8"
        };
        String bgImageUrl[] = {
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1536139507/1/Channel%20Poster/tvone_x6iblp.jpg",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275130/1/Channel%20Poster/homeliving_rc23rd.png",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1536140236/1/Channel%20Poster/euronews_r9xfyu.png",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1536139748/1/Channel%20Poster/dangdut_kgwpdm.jpg",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275130/1/Channel%20Poster/homeliving_rc23rd.png",
        };
        String cardImageUrl[] = {
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1540881207/1/Coming%20Soon%20Poster/vod-coming-soon_nfgthg.jpg",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275130/1/Channel%20Poster/homeliving_rc23rd.png",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1536140236/1/Channel%20Poster/euronews_r9xfyu.png",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275130/1/Channel%20Poster/homeliving_rc23rd.png",
                "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275130/1/Channel%20Poster/homeliving_rc23rd.png"
        };

        for (int index = 0; index < title.length; ++index) {
            list.add(
                    buildMovieInfo(
                            title[index],
                            description,
                            studio[index],
                            videoUrl[index],
                            cardImageUrl[index],
                            bgImageUrl[index]));
        }

        return list;
    }

    private static Movie buildMovieInfo(
            String title,
            String description,
            String studio,
            String videoUrl,
            String cardImageUrl,
            String backgroundImageUrl) {
        Movie movie = new Movie();
        movie.setId(count++);
        movie.setTitle(title);
        movie.setDescription(description);
        movie.setStudio(studio);
        movie.setCardImageUrl(cardImageUrl);
        movie.setBackgroundImageUrl(backgroundImageUrl);
        movie.setVideoUrl(videoUrl);
        return movie;
    }
}