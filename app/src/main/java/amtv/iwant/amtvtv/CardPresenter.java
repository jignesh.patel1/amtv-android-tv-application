/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package amtv.iwant.amtvtv;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v17.leanback.widget.BaseCardView;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import amtv.iwant.amtvtv.data.network.model.collection.Content;
import amtv.iwant.amtvtv.util.ImageUtils;

/*
 * A CardPresenter is used to generate Views and bind Objects to them on demand.
 * It contains an Image CardView
 */
public class CardPresenter extends Presenter {
    private static final String TAG = "CardPresenter";

    private static final int CARD_WIDTH = 270;
    private static final int CARD_HEIGHT = 150;

    private static final int CARD_WIDTH_COLLECTION = 270;
    private static final int CARD_HEIGHT_COLLECTION = 300;


    private static int sSelectedBackgroundColor;
    private static int sDefaultBackgroundColor;
    private Drawable mDefaultCardImage;

    //ImageCardView imageCardView;

    private static void updateCardBackgroundColor(ImageCardView view, boolean selected) {
        int color = selected ? sSelectedBackgroundColor : sDefaultBackgroundColor;
        // Both background colors should be set because the view's background is temporarily visible
        // during animations.
        view.setBackgroundColor(color);
        view.findViewById(R.id.info_field).setBackgroundColor(color);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent) {
        Log.d(TAG, "onCreateViewHolder");

        sDefaultBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.white);
        sSelectedBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.selected_background);
        /*
         * This template uses a default image in res/drawable, but the general case for Android TV
         * will require your resources in xhdpi. For more information, see
         * https://developer.android.com/training/tv/start/layouts.html#density-resources
         */
        mDefaultCardImage = ContextCompat.getDrawable(parent.getContext(), R.drawable.app_icon_your_company);


        /*YourCardView cardView = new YourCardView(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                super.setSelected(selected);
                imageCardView = (ImageCardView) parent.findViewById(R.id.img_collection_item);
                updateCardBackgroundColor(imageCardView, selected);
            }
        };*/

        ImageCardView cardView =  new ImageCardView(parent.getContext()) {
                    @Override
                    public void setSelected(boolean selected) {
                        updateCardBackgroundColor(this, selected);
                        super.setSelected(selected);
                    }
                };
        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        parent.setFocusable(true);
        /*if(null != imageCardView ) {
            updateCardBackgroundColor(imageCardView, false);
        }*/
        updateCardBackgroundColor(cardView,false);

        ((TextView) cardView.findViewById(R.id.title_text)).setTextColor(Color.BLACK); // Title text
        ((TextView) cardView.findViewById(R.id.content_text)).setTextColor(Color.BLACK); // Description text
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        Content content = (Content) item;
        /*ImageCardView cardView;
        if (((Content) item).getContentType().equals("VOD")) {
            cardView = (ImageCardView) viewHolder.view.findViewById(R.id.img_collection_item);
            viewHolder.view.findViewById(R.id.img_channel_item).setVisibility(View.GONE);
            cardView.setMainImageDimensions(CARD_WIDTH_COLLECTION, CARD_HEIGHT_COLLECTION);
            cardView.setMainImageScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            cardView = (ImageCardView) viewHolder.view.findViewById(R.id.img_channel_item);
            viewHolder.view.findViewById(R.id.img_collection_item).setVisibility(View.GONE);
            cardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
            cardView.setMainImageScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }*/

        ImageCardView cardView = (ImageCardView) viewHolder.view;

        cardView.setVisibility(View.VISIBLE);

        if (!((Content) item).getContentType().equals("VOD")) {
            cardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
            cardView.setMainImageScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }else{
            cardView.setMainImageDimensions(CARD_WIDTH_COLLECTION, CARD_HEIGHT_COLLECTION);
        }

        Log.d(TAG, "onBindViewHolder");
        if (content.getImageURL() != null) {
            cardView.setTitleText(content.getTitle());
            cardView.setContentText(content.getTitle());
           /* Glide.with(viewHolder.view.getContext())
                    .load(updatedUrl(viewHolder.view.getContext(), content.getImageURL()))
                    .centerCrop()
                    .error(mDefaultCardImage)
                    .into(cardView.getMainImageView());*/
            Picasso.with(viewHolder.view.getContext()).load(ImageUtils.updatedUrl(viewHolder.view.getContext(), content.getImageURL()))
                    .placeholder(mDefaultCardImage)
                    .error(mDefaultCardImage)
                    .into(cardView.getMainImageView());

            cardView.setBackgroundColor(Color.WHITE);

        }
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        Log.d(TAG, "onUnbindViewHolder");
        ImageCardView cardView = (ImageCardView) viewHolder.view;
        // Remove references to images so that the garbage collector can free up memory
        cardView.setBadgeImage(null);
        cardView.setMainImage(null);
    }

    public class YourCardView extends BaseCardView {

        public YourCardView(Context context) {
            super(context, null, android.support.v17.leanback.R.attr.imageCardViewStyle);
            LayoutInflater.from(getContext()).inflate(R.layout.r_collection_item, this);
            setFocusable(true);
        }
    }

}
