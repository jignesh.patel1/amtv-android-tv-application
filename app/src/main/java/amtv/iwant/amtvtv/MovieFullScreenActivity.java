package amtv.iwant.amtvtv;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.underscore.Predicate;
import com.github.underscore.U;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import io.reactivex.android.schedulers.AndroidSchedulers;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import amtv.iwant.amtvtv.cast.DemoUtil;
import amtv.iwant.amtvtv.cast.PlayerManager;
import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.RestErrorConsumer;
import amtv.iwant.amtvtv.data.network.RestSuccessConsumer;
import amtv.iwant.amtvtv.data.network.event.RestError;
import amtv.iwant.amtvtv.data.network.model.collection.CollectionRes;
import amtv.iwant.amtvtv.data.network.model.collection.Content;
import amtv.iwant.amtvtv.data.repository.BaseRepository;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MovieFullScreenActivity extends AppCompatActivity implements
        ExoPlayer.EventListener, PlayerControlView.VisibilityListener,
        PlayerManager.QueuePositionListener {

    @BindView(R.id.player_view)
    PlayerView playerView;

    @BindView(R.id.root)
    FrameLayout root;

    @BindView(R.id.ll_menu)
    LinearLayout llMenu;

   /* @BindView(R.id.txt_tvri)
    AppCompatTextView txtTvri;

    @BindView(R.id.txt_tvone)
    AppCompatTextView txtTvOne;*/

    @BindView(R.id.listview)
    ListView listView;

    private static int cacheID;
    public static final String TAG = "AMTV_TAB_MOVIE_PLAYER";

    private DefaultTrackSelector trackSelector;
    private DefaultRenderersFactory renderersFactory;
    private MediaSource videoSource;
    private SimpleExoPlayer exoPlayer;

    // private String vURL = "";
    private final String proxyURL = "https://cors-anywhere.herokuapp.com/";
    private boolean useProxy = false;
    private DefaultLoadControl loadControl;
    private DebugTextViewHelper debugViewHelper;

    private DataSource.Factory mediaDataSourceFactory;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private String userAgent;

    // Cache helper Properties
    private File downloadDirectory;
    private Cache downloadCache;
    SimpleCache sDownloadCache = null;

    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private static final String DOWNLOAD_CONTENT_DIRECTORY = "downloads";

    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    //  private static final String KEY_AUTO_PLAY = "auto_play

    private TextView textBandwidth;
    private TextView textStatus;
    private TextView textTrackInfo;

    private int startWindow;
    private long startPosition;
    private boolean startAutoPlay;
    private Context context;
    private String movieUrl= "";
    private MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder mStateBuilder;
    //private int pos = 0;

    String[] content;
    int pos = 0;
    boolean fullScreenLauncher = false;

    public DataManager dataManager = null;
    public CompositeDisposable compositeDisposable = null;
    public BaseRepository baseRepository = null;
    private List<Content> list;
    private String firstContentUrl;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_video_player);
        ButterKnife.bind(this);
        setUp(savedInstanceState);
        setListner();
    }

    private void setListner() {

        listView.setItemsCanFocus(true);
        String[] values = new String[]{"" +
                "TVRI",
                "TvOne",
                "Euro News",
                "Metro Tv"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, android.R.id.text1, values);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /*txtTvri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/qZYCrrSZ/qZYCrrSZ.m3u8"));
                exoPlayer.prepare(videoSource);

                llMenu.setVisibility(View.GONE);
            }
        });

        txtTvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/NXoBGxyj/NXoBGxyj.m3u8"));
                exoPlayer.prepare(videoSource);

                llMenu.setVisibility(View.GONE);
            }
        });*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (fullScreenLauncher) {

            MovieFullScreenActivity.this.finish();
            startActivity(new Intent(this, MainActivity.class));

        } else {

            if (exoPlayer != null) {
                // exoPlayer.setPlayWhenReady(false);
                Log.d(TAG, "RELEASE PLAYER");
                releasePlayer("Activity: EXIT_MOVIE_PLAY");
            } else {
                Log.d(TAG, "PLAYR IS NULL");
            }
            finish();
        }
    }

    private void setUp(Bundle savedInstanceState) {
        context = MovieFullScreenActivity.this;
        dataManager = new AppDataManager(context);
        compositeDisposable = new CompositeDisposable();
        baseRepository = new BaseRepository();
        dataManager.setBaseUrl();
        userAgent = Util.getUserAgent(context, "iWant SmartPlay");

        //castContext = com.google.android.gms.cast.framework.CastContext.getSharedInstance(context);

        if (null != getIntent().getExtras()) {
            //movieUrl = getIntent().getExtras().getString("MovieUrl");
            //movieUrl = "http://edge.linknetott.swiftserve.com/live/BSgroup/amlst:dangdutch/playlist.m3u8";

            if (getIntent().hasExtra(DetailsActivity.MOVIE)) {
                content = getIntent().getStringArrayExtra(DetailsActivity.MOVIE);
                pos = getIntent().getIntExtra("pos", 0);
            } else {
                movieUrl = getIntent().getExtras().getString("URL");
            }
            // movieUrl = movieUrl.replace("http://ott.amdc.tech:8080/","");
        }

        Log.v("log"," movieUrl ==>" + movieUrl);

        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = true; //savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            Log.d(TAG, "Resume from Saved Instance:" + startPosition + "," + startWindow);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            Log.d(TAG, "Starting New Track");
            clearStartPosition("On Create();");
        }
        /*
        Commeting the Thread Part, Rather making direct invocation to avoid threat of open thread upon release.
        SmartPlayer playerThread = new SmartPlayer();
        playerThread.start();
        */

        if (null == movieUrl || movieUrl.equals("")) {
            callApi();
            fullScreenLauncher = true;
        }else {
            initPlayback();
        }
    }
/*
    private class SmartPlayer extends Thread {
        public void run() {
            createPlayer();
        }
    }
    */

    private void createPlayer() {
        // Binding the Player View Object from Resource
        playerView.setShowBuffering(false);
        //playerView.setResizeMode(3);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);


    }

    private void initPlayback() {

        //  useProxy = switchProxy.isChecked();
        useProxy = false;
        renderersFactory = null;
        renderersFactory = new DefaultRenderersFactory(this,
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER);

        // Step 4: Crate Default Load Control
        loadControl = null;
        loadControl = new DefaultLoadControl.Builder().createDefaultLoadControl();

        // Step 5: Create Track selector

        trackSelector = null;
        trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(BANDWIDTH_METER));


        //Log.d(TAG, "Forcing Lowest bitrate");
        DefaultTrackSelector.Parameters newParam = trackSelector.getParameters().buildUpon().build();
        trackSelector.setParameters(newParam);

        //  currentView.setText("Loading.."+currentName);
        //  Log.d(TAG,"Loading.."+currentName);
        /*movieUrl = movieUrl.replace("http://ott.amdc.tech:8080/", "");

        String videoSourceURI = (useProxy) ? proxyURL + movieUrl : movieUrl;
        Log.d(TAG, "URI:" + videoSourceURI);*/
        Log.d(TAG, "Creating Cache Data Source");

        resetDataSource();

        Log.d(TAG, "Creating HLS Media Source Source");


        //if (null != videoSourceURI && !videoSourceURI.isEmpty()) {
        //    videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(videoSourceURI));
        MediaSource mediaSource = null;

        if (null != content && content.length > 0) {

            ArrayList<String> len = new ArrayList<>(U.filter(Arrays.asList(content), new Predicate<String>() {
                public boolean test(String item) {
                    return null != item && !item.isEmpty();
                }
            }));

            MediaSource[] mediaSources = new MediaSource[len.size()];

            for (int i = 0; i < content.length; i++) {
                if (null != content[i] && content[i].length() > 0) {
                    movieUrl = content[i].replace("http://ott.amdc.tech:8080/", "");
                    mediaSources[i] = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(movieUrl));
                }
            }

            mediaSource =
                    mediaSources.length == 1 ? mediaSources[0] : new ConcatenatingMediaSource(mediaSources);
        } else {
            movieUrl = movieUrl.replace("http://ott.amdc.tech:8080/", "");
            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(movieUrl));
        }

        Log.v("log" , " movieUrl" + movieUrl);
           /* DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

            DataSource.Factory 	dataSourceFactory = new DefaultDataSourceFactory(context,
                    Util.getUserAgent(context, "streamradio"), bandwidthMeter);

            videoSource = new ExtractorMediaSource(
                    Uri.parse(videoSourceURI), dataSourceFactory,
                    extractorsFactory, null, null);*/

        //videoSource = new ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(videoSourceURI));
        //}

        //Step 6: // Create Exoplayer
        Log.d(TAG, "Creating Player:");
        exoPlayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        Log.d(TAG, "Player Created Successfully with Default Tract Selector");
        // Step 7: Attach Media Source with Exo Player
        playerView.setPlayer(exoPlayer);
        //  playerView.setPlaybackPreparer(this);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        playerView.requestFocus();

        playerView.setControllerVisibilityListener(this);


        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            exoPlayer.seekTo(startWindow, startPosition);
        }

        if (null != mediaSource) {
            exoPlayer.prepare(mediaSource, !haveStartPosition, false);
            //  playerView.setUseController(false);
        } else {
            Log.d(TAG, "Media Source is Null");
            exoPlayer.prepare(videoSource);
            //  playerView.setUseController(true);
        }


        // Step 9: Set the player for Autoplay as soon as buffer is available to play
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_DEFAULT);
        exoPlayer.addListener(this);

        exoPlayer.seekToDefaultPosition(pos);

    }

    private void resetDataSource() {
        HttpDataSource.Factory upstreamFactory = new DefaultHttpDataSourceFactory(userAgent, BANDWIDTH_METER);
        if (useProxy) {
            Log.d(TAG, "Http Proxy Streaming Enabled");
            upstreamFactory.getDefaultRequestProperties().set("X-Requested-With", "HttpRequest");
        }
        //Step 2.2. Create the Cache Source Factory with Finding the HTTP Data Source Factory

        CacheDataSourceFactory cacheFactory = new CacheDataSourceFactory(getDownloadCache(),
                upstreamFactory,
                new FileDataSourceFactory(),
                null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                null);

        mediaDataSourceFactory = cacheFactory;
    }

    /*
   This section is for the cache manager only
    */

    private synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            Log.d(TAG, "downloadCache=null:" + downloadContentDirectory.getAbsolutePath());
            Log.d(TAG, "Before cacheID:" + cacheID);

            if (null != downloadContentDirectory)
                try {
                    downloadCache = new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor());
                } catch (IllegalStateException e) {
                    cacheID = cacheID++;
                    Log.d(TAG, "After cacheID:" + cacheID);
                    File newdownloadContentDirectory = new File(getDownloadDirectory(), "ON_ERROR_HOME" + Math.random());
                    downloadCache = new SimpleCache(newdownloadContentDirectory, new NoOpCacheEvictor());
                    Log.d(TAG, "IGNORING CACHE ERROR");
                }
        } else {
            Log.d(TAG, "Cache is Available");
        }
        return downloadCache;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = getFilesDir();
            }
        }
        return downloadDirectory;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                !playWhenReady) {

            playerView.setKeepScreenOn(false);
        } else { // STATE_IDLE, STATE_ENDED
            // This prevents the screen from getting dim/lock
            playerView.setKeepScreenOn(true);
        }

        if (playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_NEXT) {
            //do something
            Toast.makeText(context, " Next Button Press ==> ", Toast.LENGTH_SHORT).show();
        }
        if (playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS) {
            //do something else
            Toast.makeText(context, " Prev Button Press ==> ", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        //  Toast.makeText(getApplicationContext(),"Error! Please check internet connection.",Toast.LENGTH_LONG).show();
        Log.d(TAG, "ERROR - MOVIE PLAY");
        if (exoPlayer != null) {
            updateStartPosition();
            //releasePlayer(":OnPlayerError():");
            if (videoSource != null && exoPlayer != null) {
                Log.d(TAG, "ERROR - MOVIE PLAY : ATTEMPTING TO RESUME");
                exoPlayer.prepare(videoSource, false, false);
            } else {
                Log.d(TAG, "ERROR - MOVIE PLAY : INITPLAYBACK");
                initPlayback();
            }
        }

    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        updateStartPosition();
    }

    @Override
    public void onSeekProcessed() {

    }


    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            Log.d(TAG, "OnStart() -> SDK IS OK : Util.SDK_INT:" + Util.SDK_INT);
            //initPlayback();
        } else {
            Log.d(TAG, "OnStart() -> SDK IS NOT OK : Util.SDK_INT:" + Util.SDK_INT);
        }
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "MOVIE_PLAY_ON_STOP");
        super.onStop();
        if (exoPlayer != null) {
            //exoPlayer.setPlayWhenReady(false);
            releasePlayer("Activity: MOVIE_PLAY_ON_STOP");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "MOVIE_PLAY_ON_PAUSE");
        // if (Util.SDK_INT <= 23) {

        releasePlayer("Activity: MOVIE_PLAY_ON_PAUSE");
        //  }
        logPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "MOVIE_PLAY_ON_RESUME");
        //   if (Util.SDK_INT <= 23 || exoPlayer == null) {
        Log.d(TAG, "MOVIE_PLAY_ON_RESUME");
        updateStartPosition();
        //initPlayback();
        //   }
        logPosition();
    }

    private void releasePlayer(String event) {
        Log.d(TAG, event);
        if (exoPlayer != null) {
            updateTrackSelectorParameters();
            updateStartPosition();
            videoSource = null;
            exoPlayer.release();
            exoPlayer = null;
            logPosition();
        } else {
            Log.d(TAG, "RELEASE_PLAYER-> EXO-PLAYER IS NULL");
        }
    }

    private void updateTrackSelectorParameters() {
        Log.d(TAG, "updateTrackSelectorParameters()");
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }

    private void clearStartPosition(String event) {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
        Log.d(TAG, "Clear Start Position: Event" + event);
        logPosition();
    }

    /*
   Additional Functions for smooth plaback
    */
    private void updateStartPosition() {
        if (exoPlayer != null) {
            startAutoPlay = exoPlayer.getPlayWhenReady();
            startWindow = exoPlayer.getCurrentWindowIndex();
            startPosition = Math.max(0, exoPlayer.getContentPosition());
            Log.d(TAG, "StartPosition Updated:" + startPosition + "," + startWindow);
        } else {
            Log.d(TAG, "updateStartPosition() without anything");
        }
    }

    private void logPosition() {
        Log.d(TAG, "Start Position:" + startPosition);
        Log.d(TAG, "Start Window:" + startWindow);
        Log.d(TAG, "Start Auto Play:" + startAutoPlay);
    }

    @Override
    public void onVisibilityChange(int visibility) {


    }

    @Override
    public void onQueuePositionChanged(int previousIndex, int newIndex) {
        if (previousIndex != C.INDEX_UNSET) {
            // mediaQueueListAdapter.notifyItemChanged(previousIndex);
        }
        if (newIndex != C.INDEX_UNSET) {
            // mediaQueueListAdapter.notifyItemChanged(newIndex);
        }
    }

    // Activity input

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // See whether the player view wants to handle media or DPAD keys events.
        KeyEvent keyEvent = (KeyEvent) event;


        if (null != content && content.length > 0) {

            int press = keyEvent.getKeyCode();
            Log.d("test", String.valueOf(press));

            if (event.getAction() != KeyEvent.ACTION_DOWN)
                return true;

            switch (press) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    // Do something for LEFT direction press
                    Log.d("test", "LEFT");

                    if (pos > 0) {
                        pos = pos - 1;
                        exoPlayer.seekToDefaultPosition(pos);
                    } else {
                        finish();
                    }

                    return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    // Do something for RIGHT direction press

                    Log.d("test", "RIGHT");
                    if (pos < 5) {
                        pos = pos + 1;
                        exoPlayer.seekToDefaultPosition(pos);
                    } else {
                        finish();
                    }

                    return true;

                case KeyEvent.KEYCODE_DPAD_CENTER:

                    //playerView.showController();
                    if (llMenu.getVisibility() != View.VISIBLE) {
                        llMenu.setVisibility(View.VISIBLE);
                    } else {
                        if (listView.getSelectedItemPosition() == 0) {
                            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/qZYCrrSZ/qZYCrrSZ.m3u8"));
                        } else if (listView.getSelectedItemPosition() == 1) {
                            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/NXoBGxyj/NXoBGxyj.m3u8"));
                        } else if (listView.getSelectedItemPosition() == 2) {
                            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/4t68X4Ay/4t68X4Ay.m3u8"));
                        } else if (listView.getSelectedItemPosition() == 3) {
                            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse("http://103.214.185.98/zGZo46sQ/zGZo46sQ.m3u8"));
                        }


                        exoPlayer.prepare(videoSource);

                        llMenu.setVisibility(View.GONE);
                    }

                    return true;

                case KeyEvent.KEYCODE_BACK:

                    finish();

                default:
                    //return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
                    return super.dispatchKeyEvent(event);

            }
        } else {
            /*if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT) {

                pos = pos + 1;
                exoPlayer.seekToDefaultPosition(pos);

                return true;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT) {

                pos = pos - 1;
                exoPlayer.seekToDefaultPosition(pos);

                return true;
            } else {
                return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
            }*/
            return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
        }


    }



    private void callApi() {
        compositeDisposable.add(baseRepository.collectionList("", dataManager)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSuccessConsumer<CollectionRes>() {
                    @Override
                    public void success(@NonNull CollectionRes collectionRes) {


                        if (null != collectionRes) {

                            int i;
                            for (i = 0; i < collectionRes.getDocs().size(); i++) {

                                list = collectionRes.getDocs().get(i).getContents();
                                if (i != 0) {
                                    Collections.shuffle(list);
                                }

                                //firstContentUrl =  list.get(0).getContentUrl();
                                movieUrl = list.get(0).getContentUrl();

                                break;
                            }

                        }

                        initPlayback();

                    }
                }, new RestErrorConsumer() {
                    @Override
                    public void error(RestError restError) {
                        Toast.makeText(context,restError.getError().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }));

    }


}
