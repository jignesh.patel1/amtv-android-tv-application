/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package amtv.iwant.amtvtv;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;
import android.text.Html;

import amtv.iwant.amtvtv.data.network.model.collection.Content;

public class DetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Content movie = (Content) item;

        if (movie != null) {
            viewHolder.getTitle().setText(movie.getTitle());
            //viewHolder.getSubtitle().setText(Html.fromHtml(movie.getDuration()));
            if(null != movie.getDescription() && !movie.getDescription().equalsIgnoreCase("")) {
                viewHolder.getBody().setText(Html.fromHtml(movie.getDescription()));
            }
        }
    }
}
