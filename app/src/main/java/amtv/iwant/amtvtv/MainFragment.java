/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package amtv.iwant.amtvtv;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.RestErrorConsumer;
import amtv.iwant.amtvtv.data.network.RestSuccessConsumer;
import amtv.iwant.amtvtv.data.network.event.RestError;
import amtv.iwant.amtvtv.data.network.model.collection.CollectionRes;
import amtv.iwant.amtvtv.data.network.model.collection.Content;
import amtv.iwant.amtvtv.data.repository.BaseRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainFragment extends BrowseFragment {
    private static final String TAG = "MainFragment";

    private static final int BACKGROUND_UPDATE_DELAY = 300;
    private static final int GRID_ITEM_WIDTH = 200;
    private static final int GRID_ITEM_HEIGHT = 200;
    private static final int NUM_ROWS = 6;
    private static final int NUM_COLS = 9;

    private final Handler mHandler = new Handler();
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private Timer mBackgroundTimer;
    private String mBackgroundUri;
    private BackgroundManager mBackgroundManager;

    public DataManager dataManager = null;
    public CompositeDisposable compositeDisposable = null;
    public BaseRepository baseRepository = null;
    private Context context = null;

    List<Content> list = new ArrayList<>();
    List<Content> listCurrent;
    List<Content> liveChannelList = new ArrayList<>();
    private CollectionRes collectionResList;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onActivityCreated(savedInstanceState);

        prepareBackgroundManager();

        setUp();
        setupUIElements();

        loadRows();

        setupEventListeners();
    }

    private void setUp() {
        context = getActivity();
        dataManager = new AppDataManager(context);
        compositeDisposable = new CompositeDisposable();
        baseRepository = new BaseRepository();
        dataManager.setBaseUrl();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mBackgroundTimer) {
            Log.d(TAG, "onDestroy: " + mBackgroundTimer.toString());
            mBackgroundTimer.cancel();
        }
    }

    private void loadRows() {


        compositeDisposable.add(baseRepository.collectionList("", dataManager)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RestSuccessConsumer<CollectionRes>() {
                    @Override
                    public void success(@NonNull CollectionRes collectionRes) {


                        if (null != collectionRes) {

                            // List<Movie> list = MovieList.setupMovies();
                            collectionResList = collectionRes;

                            ArrayObjectAdapter rowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
                            CardPresenter cardPresenter = new CardPresenter();

                            int i;
                            for (i = 0; i < collectionRes.getDocs().size(); i++) {

                                list = collectionRes.getDocs().get(i).getContents();
                                if (i != 0) {
                                    Collections.shuffle(list);
                                }

                                Content content = new Content();
                                content.setTitle("TVRI");
                                content.setDescription("TVRI");
                                content.setImageURL("https://res.cloudinary.com/amtv-cdn/image/upload/v1542275123/1/Channel%20Poster/tvri_t1sopu.png");
                                content.setContentUrl("http://45.251.72.188/qZYCrrSZ/qZYCrrSZ.m3u8");
                                content.setContentType("Channel");

                                ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(cardPresenter);
                                for (int j = 0; j < list.size(); j++) {
                                    /*if (null != list.get(j) && i == 0) {
                                        if (list.get(j).getTitle().equalsIgnoreCase("BaritaSatu")) {
                                            listRowAdapter.add(list.get(j));
                                            liveChannelList.add(list.get(j));
                                        } else if (list.get(j).getTitle().equalsIgnoreCase("Foodie")) {
                                            listRowAdapter.add(list.get(j));
                                            liveChannelList.add(list.get(j));
                                        } else if (list.get(j).getTitle().equalsIgnoreCase("Euro News")) {
                                            listRowAdapter.add(list.get(j));
                                            liveChannelList.add(list.get(j));
                                        } else if (list.get(j).getTitle().equalsIgnoreCase("Metro Tv")) {
                                            listRowAdapter.add(list.get(j));
                                            liveChannelList.add(list.get(j));
                                        } else if (list.get(j).getTitle().equalsIgnoreCase("TV One")) {
                                            listRowAdapter.add(list.get(j));
                                            liveChannelList.add(list.get(j));
                                        }
                                    } else if (null != list.get(j)) {*/
                                        listRowAdapter.add(list.get(j));
                                    //}
                                }

                                /*if (i == 0) {
                                    listRowAdapter.add(content);
                                    liveChannelList.add(content);
                                }*/


                                HeaderItem header = new HeaderItem(i, collectionRes.getDocs().get(i).getTitle());
                                rowsAdapter.add(new ListRow(header, listRowAdapter));
                            }

                           /* HeaderItem gridHeader = new HeaderItem(i, "PREFERENCES");

                            GridItemPresenter mGridPresenter = new GridItemPresenter();
                            ArrayObjectAdapter gridRowAdapter = new ArrayObjectAdapter(mGridPresenter);
                            gridRowAdapter.add(getResources().getString(R.string.grid_view));
                            gridRowAdapter.add(getString(R.string.error_fragment));
                            gridRowAdapter.add(getResources().getString(R.string.personal_settings));
                            rowsAdapter.add(new ListRow(gridHeader, gridRowAdapter));*/

                            setAdapter(rowsAdapter);
                        }

                    }
                }, new RestErrorConsumer() {
                    @Override
                    public void error(RestError restError) {
                        Timber.d(restError.getError().getMessage());
                    }
                }));

    }

    private void prepareBackgroundManager() {

        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());

        mDefaultBackground = ContextCompat.getDrawable(getActivity(), R.drawable.default_background);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void setupUIElements() {
        setBadgeDrawable(getActivity().getResources().getDrawable(R.drawable.banner));
        setTitle(getString(R.string.browse_title)); // Badge, when set, takes precedent
        // over title
        setHeadersState(HEADERS_ENABLED);
        setHeadersTransitionOnBackEnabled(true);


        // set fastLane (or headers) background color
        setBrandColor(ContextCompat.getColor(getActivity(), R.color.fastlane_background));
        // set search icon color
        setSearchAffordanceColor(ContextCompat.getColor(getActivity(), R.color.search_opaque));
    }

    private void setupEventListeners() {
        setOnSearchClickedListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Implement your own in-app search", Toast.LENGTH_LONG)
                        .show();
            }
        });

        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }

    private void updateBackground(String uri) {
        int width = mMetrics.widthPixels;
        int height = mMetrics.heightPixels;
        Glide.with(getActivity())
                .load(uri)
                .centerCrop()
                .error(mDefaultBackground)
                .into(new SimpleTarget<GlideDrawable>(width, height) {
                    @Override
                    public void onResourceReady(GlideDrawable resource,
                                                GlideAnimation<? super GlideDrawable>
                                                        glideAnimation) {
                        mBackgroundManager.setDrawable(resource);
                    }
                });
        mBackgroundTimer.cancel();
    }

    private void startBackgroundTimer() {
        if (null != mBackgroundTimer) {
            mBackgroundTimer.cancel();
        }
        mBackgroundTimer = new Timer();
        mBackgroundTimer.schedule(new UpdateBackgroundTask(), BACKGROUND_UPDATE_DELAY);
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Content) {
                Content content = (Content) item;
                Log.d(TAG, "Item: " + item.toString());
               /* Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(DetailsActivity.MOVIE, content);

                ImageCardView cardView = (ImageCardView) itemViewHolder.view.findViewById(R.id.img_collection_item);

                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                       ((ImageCardView) itemViewHolder.view).getMainImageView(),
                       // cardView,
                        DetailsActivity.SHARED_ELEMENT_NAME)
                        .toBundle();
                getActivity().startActivity(intent, bundle);*/
               int pos = 0;

                String[] uris = new String[collectionResList.getDocs().size()];
                listCurrent = new ArrayList<>();

                 for (int i = 0; i < collectionResList.getDocs().size(); i++) {
                     for(int m=0 ; m < collectionResList.getDocs().get(i).getContents().size() ; m++){
                         if(((Content) item).getTitle().equalsIgnoreCase(collectionResList.getDocs().get(i).getContents().get(m).getTitle())){
                            listCurrent = collectionResList.getDocs().get(i).getContents();
                        }
                     }
                 }

                /*if (content.getContentType().equalsIgnoreCase("LIVE")) {

                    for (int k = 0; k < listCurrent.size() ; k++) {

                        uris[k] = listCurrent.get(k).getContentUrl();
                        if(((Content) item).getTitle().equalsIgnoreCase(listCurrent.get(k).getTitle())){
                            pos = k;
                        }

                        //uris[5] = "https://res.cloudinary.com/amtv-cdn/image/upload/v1542275123/1/Channel%20Poster/tvri_t1sopu.png";

                    }
                    Intent intent = new Intent(getActivity(), MovieFullScreenActivity.class);
                    intent.putExtra(DetailsActivity.MOVIE, uris);
                    intent.putExtra("pos",pos);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), MovieFullScreenActivity.class);
                    intent.putExtra("URL", content.getContentUrl());
                    startActivity(intent);
                }*/

                Intent intent = new Intent(getActivity(), MovieFullScreenActivity.class);
                intent.putExtra("URL", content.getContentUrl());
                startActivity(intent);

            } else if (item instanceof String) {
                if (((String) item).contains(getString(R.string.error_fragment))) {
                    Intent intent = new Intent(getActivity(), BrowseErrorActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), ((String) item), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(
                Presenter.ViewHolder itemViewHolder,
                Object item,
                RowPresenter.ViewHolder rowViewHolder,
                Row row) {
            if (item instanceof Movie) {
                mBackgroundUri = ((Movie) item).getBackgroundImageUrl();
                startBackgroundTimer();
            }
        }
    }

    private class UpdateBackgroundTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateBackground(mBackgroundUri);
                }
            });
        }
    }

    private class GridItemPresenter extends Presenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            TextView view = new TextView(parent.getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(GRID_ITEM_WIDTH, GRID_ITEM_HEIGHT));
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            view.setBackgroundColor(
                    ContextCompat.getColor(getActivity(), R.color.default_background));
            view.setTextColor(Color.WHITE);
            view.setGravity(Gravity.CENTER);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ((TextView) viewHolder.view).setText((String) item);
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {
        }
    }

}
