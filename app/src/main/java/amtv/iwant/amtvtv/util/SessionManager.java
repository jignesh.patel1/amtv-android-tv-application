package amtv.iwant.amtvtv.util;

/**
 * Created by ADMIN on 7/5/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class SessionManager {
    private static final String KEY_BUSINESS_LOGO_URL = "KEY_BUSINESS_LOGO_URL";

    private static final String PREF_NAME = "LoginSession";

    private static final String KEY_BUSINESS_ABOUT_APP = "KEY_BUSINESS_ABOUT_APP";
    private static final String KEY_ABOUT_BANNER_URL = "KEY_ABOUT_BANNER_URL";

    private static final String KEY_BUSINESS_DEVELOP_BY = "KEY_BUSINESS_DEVELOP_BY";
    private static final String KEY_BUSINESS_DEVELOP_BY_IMAGE_URL = "KEY_BUSINESS_DEVELOP_BY_IMAGE_URL";

    private static final String KEY_BUSINESS_MARKET_BY = "KEY_BUSINESS_MARKET_BY";
    private static final String KEY_BUSINESS_MARKET_BY_IMAGE_URL = "KEY_BUSINESS_MARKET_BY_IMAGE_URL";

    private static final String KEY_BUSINESS_ONLINE_PAYMENT = "KEY_BUSINESS_ONLINE_PAYMENT";

    private static final String KEY_BUSINESS_PRIVACY_POLICY_URL = "KEY_BUSINESS_PRIVACY_POLICY_URL";
    private static final String KEY_BUSINESS_TERMS_AND_CONDITION_URL = "KEY_BUSINESS_TERMS_AND_CONDITION_URL";


    private static final String KEY_BUSINESS_REFUND_POLICY_URL = "KEY_BUSINESS_REFUND_POLICY_URL";
    private static final String KEY_BUSINESS_RETURN_CANCELLATION_POLICY_URL = "KEY_BUSINESS_RETURN_CANCELLATION_POLICY_URL";
    private static final String KEY_BUSINESS_SHIPPING_POLICY_URL = "KEY_BUSINESS_SHIPPING_POLICY_URL";

    private static final String KEY_BUSINESS_LATITUDE = "KEY_BUSINESS_LATITUDE";
    private static final String KEY_BUSINESS_LONGITUDE = "KEY_BUSINESS_LONGITUDE";

    private static final String KEY_BUSINESS_TITLE = "KEY_BUSINESS_TITLE";

    private static final String KEY_BUSINESS_DESCRIPTION = "KEY_BUSINESS_DESCRIPTION";
    private static final String KEY_BUSINESS_WORK_HOUR = "KEY_BUSINESS_WORK_HOUR";
    private static final String KEY_BUSINESS_MOBILE_NUMBER = "KEY_BUSINESS_MOBILE_NUMBER";
    private static final String KEY_BUSINESS_ADDRESS = "KEY_BUSINESS_ADDRESS";
    private static final String KEY_BUSINESS_HEADER_COLLECTION = "KEY_BUSINESS_HEADER_COLLECTION";

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    private static Context context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared Preferences
    private SharedPreferences pref;
    private Editor editor;

    public SessionManager(Context context) {
        if (this.context == null) {
            this.context = context;
        } else {
        }
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void storeAboutApp(String privacyPolicyUrl , String termsAndConditionUrl , String shippingUrl , String refundPolicyUrl , String returnCancellationPolicyUrl ) {

        editor.putString(KEY_BUSINESS_PRIVACY_POLICY_URL, privacyPolicyUrl);
        editor.putString(KEY_BUSINESS_TERMS_AND_CONDITION_URL, termsAndConditionUrl);

        editor.putString(KEY_BUSINESS_SHIPPING_POLICY_URL, shippingUrl);
        editor.putString(KEY_BUSINESS_REFUND_POLICY_URL, refundPolicyUrl);
        editor.putString(KEY_BUSINESS_RETURN_CANCELLATION_POLICY_URL, returnCancellationPolicyUrl);

        // commit changes
        editor.commit();

    }

    public void storeAboutBusiness(String aboutBannerUrl, String logoUrl , String aboutApp ,
                                   String developBy , String developeByImageUrl , String marketedBy ,
                                   String marketedByImageUrl , String onlinePayment, String headerCollection) {

        editor.putString(KEY_ABOUT_BANNER_URL, aboutBannerUrl);
        editor.putString(KEY_BUSINESS_LOGO_URL, logoUrl);
        editor.putString(KEY_BUSINESS_ABOUT_APP, aboutApp);
        editor.putString(KEY_BUSINESS_DEVELOP_BY, developBy);
        editor.putString(KEY_BUSINESS_DEVELOP_BY_IMAGE_URL, developeByImageUrl);
        editor.putString(KEY_BUSINESS_MARKET_BY, marketedBy);
        editor.putString(KEY_BUSINESS_MARKET_BY_IMAGE_URL, marketedByImageUrl);
        editor.putString(KEY_BUSINESS_ONLINE_PAYMENT , onlinePayment);
        editor.putString(KEY_BUSINESS_HEADER_COLLECTION , headerCollection);

        // commit changes
        editor.commit();

    }

    public void storeBusinessData(double latitude, double longitude, String businessTitle, String description,
                                  String workHours , String mobileNumber , String address) {

        editor.putString(KEY_BUSINESS_LATITUDE, String.valueOf(latitude));
        editor.putString(KEY_BUSINESS_LONGITUDE, String.valueOf(longitude));
        editor.putString(KEY_BUSINESS_TITLE, businessTitle);

        editor.putString(KEY_BUSINESS_DESCRIPTION , description);
        editor.putString(KEY_BUSINESS_WORK_HOUR , workHours);
        editor.putString(KEY_BUSINESS_MOBILE_NUMBER , mobileNumber);
        editor.putString(KEY_BUSINESS_ADDRESS , address);

        // commit changes
        editor.commit();

    }

    public String getKeyBusinessBannerWorkHours() {
        return pref.getString(KEY_BUSINESS_WORK_HOUR, "");
    }

    public String getKeyBusinessBannerMobileNumber() {
        return pref.getString(KEY_BUSINESS_MOBILE_NUMBER, "");
    }

    public String getKeyBusinessBannerAddress() {
        return pref.getString(KEY_BUSINESS_ADDRESS, "");
    }

    public String getKeyBusinessBannerDescription() {
        return pref.getString(KEY_BUSINESS_DESCRIPTION, "");
    }

    public String getKeyAboutBannerUrl() {
        return pref.getString(KEY_ABOUT_BANNER_URL, "");
    }

    public String getKeyBusinessTitle() {
        return pref.getString(KEY_BUSINESS_TITLE, "");
    }

    public String getKeyBusinessLatitude() {
        return pref.getString(KEY_BUSINESS_LATITUDE, "");
    }

    public String getKeyBusinessLongitude() {
        return pref.getString(KEY_BUSINESS_LONGITUDE, "");
    }

    public String getKeyBusinessLogoUrl() {
        return pref.getString(KEY_BUSINESS_LOGO_URL, "");
    }

    public String getKeyBusinessPrivacyPolicyUrl() {
        return pref.getString(KEY_BUSINESS_PRIVACY_POLICY_URL, "");
    }

    public String getKeyBusinessTermsAndConditionUrl() {
        return pref.getString(KEY_BUSINESS_TERMS_AND_CONDITION_URL, "");
    }

    public String getKeyBusinessShippingPolicyUrl() {
        return pref.getString(KEY_BUSINESS_SHIPPING_POLICY_URL, "");
    }

    public String getKeyBusinessReturnCancellationPolicyUrl() {
        return pref.getString(KEY_BUSINESS_RETURN_CANCELLATION_POLICY_URL, "");
    }

    public String getKeyBusinessRefundPolicyUrl() {
        return pref.getString(KEY_BUSINESS_REFUND_POLICY_URL, "");
    }

    public String getKeyBusinessAboutApp() {
        return pref.getString(KEY_BUSINESS_ABOUT_APP, "");
    }

    public String getKeyBusinessDevelopBy() {
        return pref.getString(KEY_BUSINESS_DEVELOP_BY, "");
    }

    public String getKeyBusinessDevelopByImageUrl() {
        return pref.getString(KEY_BUSINESS_DEVELOP_BY_IMAGE_URL, "");
    }

    public String getKeyBusinessMarketBy() {
        return pref.getString(KEY_BUSINESS_MARKET_BY, "");
    }

    public String getKeyBusinessMarketByImageUrl() {
        return pref.getString(KEY_BUSINESS_MARKET_BY_IMAGE_URL, "");
    }

    public String getKeyBusinessOnlinePayment() {
        return pref.getString(KEY_BUSINESS_ONLINE_PAYMENT, "");
    }

    public String getKeyBusinessHeaderCollection() {
        return pref.getString(KEY_BUSINESS_HEADER_COLLECTION, "");
    }
}