package amtv.iwant.amtvtv.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import amtv.iwant.amtvtv.R;
import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.model.config.FilterConfigRealm;
import io.realm.RealmResults;

public class ThemeUtils
{
    private static int sTheme;
    public final static int THEME_DEFAULT = 0;
    public final static int AppThemeBlue = 1;
    public final static int THEME_BLUE = 2;

    public static String themeValue = "";

    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        RealmResults<FilterConfigRealm> list;

        DataManager dataManager = new AppDataManager(activity);

        list = dataManager.openRealm().where(FilterConfigRealm.class).findAll();

        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).getParameters().size(); j++) {
                if(list.get(i).getParameters().get(j).getName().equals("THEME_COLOR_ALIAS")) {
                    themeValue = list.get(i).getParameters().get(j).getValue();
                }
            }
        }

        dataManager.closeRealm();
    }

    public static String fetchPrimaryColor(Context context) {

        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorPrimary });
        int color = a.getColor(0, 0);

        a.recycle();

        String hexColor = String.format("#%06X", (0xFFFFFF & color));

        return hexColor;
    }
}