package amtv.iwant.amtvtv.util;

import android.content.Context;

/**
 * Created by manish on 26/3/18.
 */

public class ImageUtils {

    public static String updatedUrl(Context context, String cloudinaryUrl) {

        if(cloudinaryUrl.startsWith("https://res.cloudinary.com/")) {
            return cloudinaryUrl.replace("/upload/", "/upload/q_auto:low/");
        } else {
            return cloudinaryUrl;
        }
    }
}
