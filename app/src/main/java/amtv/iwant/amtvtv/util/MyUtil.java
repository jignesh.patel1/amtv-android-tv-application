package amtv.iwant.amtvtv.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;
import android.view.Window;

import amtv.iwant.amtvtv.R;


/**
 * Created by jignesh on 23/11/17.
 */

public class MyUtil {

    public static boolean isNotEmaptyString(String str) {
        boolean flag = false;
        if (str != null && !str.trim().isEmpty()) {
            flag = true;
        }
        return flag;
    }

    /**
     * get progressbar dialog
     * **
     */

    public static Dialog get_dialog(Context context, int layoutid) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layoutid);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.transparent)));
        dialog.setCancelable(false);
        return dialog;
    }

}
