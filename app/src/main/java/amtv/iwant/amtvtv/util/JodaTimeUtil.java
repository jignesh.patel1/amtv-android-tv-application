package amtv.iwant.amtvtv.util;

import android.text.TextUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Calendar;
import java.util.TimeZone;

import timber.log.Timber;

public class JodaTimeUtil
    {

        String dateFormat =  "yyyy-MM-dd"; //"dd/MMM/yyyy";
        String timeFormat = "hh:mm a";

        String displayDateFormat =  "dd/MM/yyyy";
        String displayTimeFormat = "hh:mm a";

        private static JodaTimeUtil instance;
        public static JodaTimeUtil getInstance()
        {
            if(instance==null)
            {
                instance = new JodaTimeUtil();
            }
            return instance;
        }

        private JodaTimeUtil()
        {
            Calendar cal = Calendar.getInstance();
            TimeZone timeZone = cal.getTimeZone();

            String systemTimeZoneId = timeZone.getID();
            Timber.d("System TimeZone ID :"+systemTimeZoneId);
            DateTimeZone.setDefault(DateTimeZone.forID(systemTimeZoneId));
        }

        public void setDateTimeZone(String timeZoneId)
        {
            DateTimeZone.setDefault(DateTimeZone.forID(timeZoneId));
        }
        public void setDateFormat(String dateFormat)
        {
            this.dateFormat = dateFormat;
        }
        public void setTimeFormat(String timeFormat)
        {
            this.timeFormat = timeFormat;
        }

        public DateTime today()
        {
           return new DateTime();
        }

        public String toDateString(DateTime dateTime)
        {
            DateTimeFormatter formatter=DateTimeFormat.forPattern(dateFormat);
            return formatter.print(dateTime);
        }

        public String toDateString(DateTime dateTime, String dateFormat)
        {
            DateTimeFormatter formatter=DateTimeFormat.forPattern(dateFormat);
            return formatter.print(dateTime);
        }

        public String toTimeString(DateTime dateTime)
        {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(timeFormat);
            String parsedTimeString = formatter.print(dateTime);
            return parsedTimeString;
        }

        public DateTime toDateTime(String dateTimeString)
        {
            DateTime dateTime;
            DateTimeFormatter formatter;
            String dateTimeFormat = dateFormat+" "+timeFormat;
            try
            {
                formatter = DateTimeFormat.forPattern(dateTimeFormat);
                dateTime = formatter.parseDateTime(dateTimeString);
            }
            catch (Exception ex)
            {
                formatter = DateTimeFormat.forPattern(dateFormat);
                dateTime = formatter.parseDateTime(dateTimeString);
            }
            return dateTime;
        }

        public DateTime toDate(String dateTimeString, String dateFormat)
        {
            DateTime dateTime;
            DateTimeFormatter formatter;
            try
            {
                formatter = DateTimeFormat.forPattern(dateFormat);
                dateTime = formatter.parseDateTime(dateTimeString);
            }
            catch (Exception ex)
            {
                formatter = DateTimeFormat.forPattern(dateFormat);
                dateTime = formatter.parseDateTime(dateTimeString);
            }
            return dateTime;
        }

        public String getUnixTimeStampToString(long unixtimeStamp)
        {
            DateTime receiveDateTime = new DateTime(unixtimeStamp * 1000);
            DateTimeFormatter formatter;
            String dateTimeFormat = dateFormat+" "+timeFormat;
            formatter = DateTimeFormat.forPattern(dateTimeFormat);
            return formatter.print(receiveDateTime);
        }

        public DateTime toDateTime(int year, int month, int day)
        {
            DateTime dateTime = new DateTime(year, month + 1, day, 0, 0);
            return dateTime;
        }

        public String toString(DateTime dateTime)
        {
            String resultString = "";
            DateTimeFormatter formatter;
            String dateTimeFormat = dateFormat+" "+timeFormat;
            formatter = DateTimeFormat.forPattern(dateTimeFormat);
            resultString = formatter.print(dateTime);
            resultString = trim12AMFromDateString(resultString);
            return resultString;
        }

        public String toString(DateTime dateTime, String dateFormat)
        {
            String resultString = "";
            DateTimeFormatter formatter;
            String dateTimeFormat = dateFormat+" "+timeFormat;
            formatter = DateTimeFormat.forPattern(dateTimeFormat);
            resultString = formatter.print(dateTime);
            resultString = trim12AMFromDateString(resultString);
            return resultString;
        }

        public DateTime fromISODateString(String dateTimeString)
        {
            DateTime dateTime = new DateTime(dateTimeString, DateTimeZone.getDefault());
            return dateTime;
        }

        public String fromISODateStrToString(String dateTimeString, String dateFormat)
        {
            DateTime dateTime;
            DateTimeFormatter formatter;
            try
            {
                dateTime = new DateTime(dateTimeString);
                if(!TextUtils.isEmpty(dateFormat))
                    formatter = DateTimeFormat.forPattern(dateFormat);
                else
                    formatter = DateTimeFormat.forPattern(displayDateFormat);
                return formatter.print(dateTime);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public String convertDateFormat(String dateTimeString)
        {
            DateTime dateTime;
            DateTimeFormatter formatter;
            try
            {
                dateTime = toDateTime(dateTimeString);
                formatter = DateTimeFormat.forPattern(displayDateFormat);
                return formatter.print(dateTime);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public String getFormattedDateByDayString(String dateTimeString)
        {
            String dateTimeFormat = dateFormat+" "+timeFormat;
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(dateTimeFormat);
            DateTimeFormatter timeFormatter = DateTimeFormat.forPattern(timeFormat);
            DateTime dateTime = toDateTime(dateTimeString);

            DateTime dateTimeInput = dateTime.withTimeAtStartOfDay();
            DateTime dateTimeToday = new DateTime().withTimeAtStartOfDay();
            DateTime dateTimeTomorrow = dateTimeToday.plusDays(1).withTimeAtStartOfDay();
            DateTime dateTimeYesterDay = dateTimeToday.minusDays(1).withTimeAtStartOfDay();

            String formattedDateString;
            if (dateTimeInput.equals(dateTimeToday))
            {
                formattedDateString = "Today at " + timeFormatter.print(dateTime);
                //TODAY
            }
            else if (dateTimeInput.equals(dateTimeTomorrow))
            {
                formattedDateString = "Tomorrow at " + timeFormatter.print(dateTime);
                //tomorrow
            }
            else if (dateTimeInput.equals(dateTimeYesterDay))
            {
                //Yester Day
                formattedDateString = "Yesterday at " + timeFormatter.print(dateTime);
            }
            else
            {
                if (dateTimeInput.getYear() == dateTimeToday.getYear())
                {
                    //YEAR is same dont show year
                    formattedDateString = dateTimeFormatter.print(dateTime);
                }
                else
                {
                    //SHow full date
                    formattedDateString = dateTimeFormatter.print(dateTime);
                }
            }
            formattedDateString = trim12AMFromDateString(formattedDateString);
            return formattedDateString;
        }

        public String singularPluralFromNumber(int number, String word)
        {
            if (number > 1)
            {
                return word + "s";
            }
            return word;
        }

        public String getTimeDiffInString(String dateTimeString, boolean isISODateTime, String prefixText, String suffixText)
        {
            String dateTimeAgoString = "";
            try
            {
                DateTime dateTime = new DateTime();
                if (isISODateTime)
                {
                    dateTime = ISODateTimeFormat.dateTime().parseDateTime(dateTimeString);
                } else
                {
                    dateTime = toDateTime(dateTimeString);
                }
                DateTime dateTimeToday = new DateTime();

                int days = Days.daysBetween(dateTime, dateTimeToday).getDays();
                if (days > 0)
                {
                    dateTimeAgoString = String.format("%s%d %s %s", prefixText, days, singularPluralFromNumber(days, "day"), suffixText);
                } else
                {
                    int hours = Hours.hoursBetween(dateTime, dateTimeToday).getHours();
                    if (hours > 0)
                    {
                        dateTimeAgoString = String.format("%s%d %s %s", prefixText, hours, singularPluralFromNumber(hours, "hour"), suffixText);
                    } else
                    {
                        int minutes = Minutes.minutesBetween(dateTime, dateTimeToday).getMinutes();
                        if (minutes > 0)
                        {
                            dateTimeAgoString = String.format("%s%d %s %s", prefixText, minutes, singularPluralFromNumber(minutes, "minute"), suffixText);
                        } else
                        {
                            dateTimeAgoString = String.format("%s%d %s %s", prefixText, "few", "seconds", suffixText);
//                        "few seconds ago";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return dateTimeAgoString;
        }

        public int getTimeDiffInDays(String dateTimeString, boolean isISODateTime)
        {
            DateTime dateTime = new DateTime();
            if(isISODateTime)
            {
                dateTime = ISODateTimeFormat.dateTime().parseDateTime(dateTimeString);
            }
            else
            {
                dateTime = toDateTime(dateTimeString);
            }
            DateTime dateTimeToday = new DateTime();
            String dateTimeAgoString;
            int days = Days.daysBetween(dateTimeToday, dateTime).getDays();

            return days;
        }

        private String trim12AMFromDateString(String formattedDateString)
        {
            String twelveAMString1 = " at 12:00 AM";
            String twelveAMString2 = " 12:00 AM";
            String twelveAMString3 = " 12:00 am";
            if (formattedDateString.contains(twelveAMString1))
            {
                formattedDateString = formattedDateString.replace(twelveAMString1, "");
            }
            else if (formattedDateString.contains(twelveAMString2))
            {
                formattedDateString = formattedDateString.replace(twelveAMString2, "");
            }
            else if (formattedDateString.contains(twelveAMString2))
            {
                formattedDateString = formattedDateString.replace(twelveAMString3, "");
            }
            return formattedDateString.trim();
        }
    }