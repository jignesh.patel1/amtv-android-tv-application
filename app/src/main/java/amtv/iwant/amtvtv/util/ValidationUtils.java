package amtv.iwant.amtvtv.util;

import android.text.TextUtils;

/**
 * Created by manish on 14/11/17.
 */

public class ValidationUtils {

    public static boolean isPasswordValid(String pwd) {

        if(!TextUtils.isEmpty(pwd)) {
            String pattern = "(?=^.{8,}$)[^\\w\\d]*(([0-9]+.*[A-Za-z]+.*)|[A-Za-z]+.*([0-9]+.*))";
            return pwd.matches(pattern);
        } else {
            return false;
        }
    }

    public static boolean isNumberOnlyValid(String mobile) {

        if(!TextUtils.isEmpty(mobile) && !mobile.contains("@")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEmailValid(String email) {

        if(!TextUtils.isEmpty(email)) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        } else {
            return false;
        }
    }



    public static boolean isEmpty(String str) {

        if(TextUtils.isEmpty(str)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPasswordSame(String password, String confirmPassword) {

        if(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword)
                && password.equals(confirmPassword)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isMobileValid(String mobileNumber){

        if(!TextUtils.isEmpty(mobileNumber)) {
            String pattern = ".{7,}$";
            return mobileNumber.matches(pattern);
        } else {
            return false;
        }
    }

}
