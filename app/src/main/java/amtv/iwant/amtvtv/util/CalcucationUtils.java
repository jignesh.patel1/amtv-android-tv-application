package amtv.iwant.amtvtv.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by manish on 27/11/17.
 */

public class CalcucationUtils {

    public static int decimalPlaces = 2;

    public static Double calculatePriceWithPercentage(Double price, Double percentageValue) {
        Double obtainPrice = 0.0;

        try {
            obtainPrice = (price * percentageValue) / 100;
            obtainPrice = round(obtainPrice, decimalPlaces);
        } catch (Exception e) {
            obtainPrice = 0.0;
        }

        return obtainPrice;

    }

    public static Double calculatePriceWithAbsolute(Double price, Double value) {
        Double obtainPrice = 0.0;

        try {
            obtainPrice = price - value;
            obtainPrice = round(obtainPrice, decimalPlaces);
        } catch (Exception e) {
            obtainPrice = 0.0;
        }

        return obtainPrice;

    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

   /* public static String currencyFormatter(Context context,Double value) {

       // Locale localeID = context.getResources().getConfiguration().locale;
        DataManager dataManager = new AppDataManager(context);
        String returnValue = "";
        String currencyName = dataManager.getSharedPreference().get(Constants.PREF_KEY_CURRENCY, "");
        Locale localeID = new Locale(dataManager.getSharedPreference().get(Constants.PREF_KEY_LANGUAGE, ""), dataManager.getSharedPreference().get(Constants.PREF_KEY_COUNTRY, ""));

        try {
            //NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
            //formatRupiah.setCurrency(Currency.getInstance("Rp"));
            //returnValue = formatRupiah.format((double) value);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(localeID);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            symbols.setCurrencySymbol("");
            formatter.setDecimalFormatSymbols(symbols);
            returnValue = formatter.format((double) value);
        } catch (Exception e) {
            returnValue = String.valueOf(value);
        }

        return returnValue;
    }

    public static String getUniCode(Context context) {
        String uniCode = "";
        DataManager dataManager = new AppDataManager(context);
        try {
            uniCode = dataManager.getSharedPreference().get(Constants.PREF_KEY_CURRENCY_SYM, "");
        } catch (Exception e) {
            uniCode = "";
        }
        return uniCode;
    }

    public static int positiveRatingCalculation( RatingsMetadata ratingsMetadata){

        int positiveRatingPercentage = 0;
        int totalRatingCount = 0;
        int positiveCount = 0;

        if(null != ratingsMetadata.getTotalReviewCount()) {
             totalRatingCount = ratingsMetadata.getTotalReviewCount();

            if(null != ratingsMetadata.getRatingFiveCount() || null != ratingsMetadata.getRatingFourCount() || null != ratingsMetadata.getRatingThreeCount()) {
                if(null != ratingsMetadata.getRatingFiveCount()){
                    positiveCount = positiveCount + ratingsMetadata.getRatingFiveCount();
                }

                if( null != ratingsMetadata.getRatingFourCount() ) {
                    positiveCount = positiveCount + ratingsMetadata.getRatingFourCount();
                }

                if( null != ratingsMetadata.getRatingThreeCount() ) {
                    positiveCount = positiveCount + ratingsMetadata.getRatingThreeCount();
                }

                positiveRatingPercentage =  ( positiveCount * 100) / totalRatingCount;
            }
        }

        return positiveRatingPercentage;
    }
*/
}
