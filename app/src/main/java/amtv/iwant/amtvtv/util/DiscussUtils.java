package amtv.iwant.amtvtv.util;

public class DiscussUtils {

    public static String getHtmlComment(String idPost, String shortName) {

        /*return "<div id='disqus_thread'></div>"
                + "<script type='text/javascript'>"
                + "var disqus_identifier = '"
                + idPost
                + "';"
                + "var disqus_shortname = '"
                + shortName
                + "';"
                + " (function() { var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;"
                + "dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';"
                + "(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq); })();"
                + "</script>";*/

        return "<div id='disqus_thread'></div>"
                + "<script>"
                + "var disqus_config = function () { "
                + "this.page.url = '"
                + idPost
                + "';"
                + "this.page.identifier = '"
                + shortName
                + "';};"
                + "(function() { "
                + "var d = document, s = d.createElement('script');"
                + "s.src = 'https://pruduct-discussion.disqus.com/embed.js';"
                + "s.setAttribute('data-timestamp', +new Date());"
                + "(d.head || d.body).appendChild(s);"
                + "})() ;"
                + "</script>"
                + "<noscript>Please enable JavaScript to view the <a href='https://disqus.com/?ref_noscript'>comments powered by Disqus.</a></noscript>";
    }
}
