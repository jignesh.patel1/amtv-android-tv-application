/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package amtv.iwant.amtvtv.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Created by Manish Patel on 16/10/17.
 */

public final class ViewUtils {

    private ViewUtils() {
        // This utility class is not publicly instantiable
    }

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static void setStrikeThru(TextView textView)  {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static void updateTextInputLayoutError(EditText editText, TextInputLayout textInputLayout, String message) {
        if(!TextUtils.isEmpty(getEditTextValue(editText))) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
          //  editText.setBackground(ContextCompat.getDrawable(editText.getContext(), R.drawable.shape_bg_rounded_corner_text_input));
        } else {
            textInputLayout.setError(message);
            textInputLayout.setErrorEnabled(true);
          //  editText.setBackground(ContextCompat.getDrawable(editText.getContext(), R.drawable.shape_bg_rounded_corner_text_input));
        }

    }

    public static void updateTextInputLayoutError(EditText editText, TextInputLayout textInputLayout, String message, boolean isValid) {
        if(isValid) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
           // editText.setBackground(ContextCompat.getDrawable(editText.getContext(), R.drawable.shape_bg_rounded_corner_text_input));
        } else {
            textInputLayout.setError(message);
            textInputLayout.setErrorEnabled(true);
          //  editText.setBackground(ContextCompat.getDrawable(editText.getContext(), R.drawable.shape_bg_rounded_corner_text_input));
        }

    }


    /*public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }*/

    /*public static void changeIconDrawableToGray(Context context, Drawable drawable) {
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(ContextCompat
                    .getColor(context, R.color.dark_gray), PorterDuff.Mode.SRC_ATOP);
        }
    }*/


    public static Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(),  "fonts/myriad_pro_regular.ttf");
    }

    public static void updateGradientDrawableBackground(View v, @ColorInt int color) {
        GradientDrawable gradientDrawable = (GradientDrawable) v.getBackground();
        gradientDrawable.setColor(color);
    }

    public static String getEditTextValue (EditText editText) {
        return editText.getText().toString().trim();
    }

}
