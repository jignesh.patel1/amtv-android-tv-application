package amtv.iwant.amtvtv.util;

import org.json.JSONException;
import org.json.JSONObject;

import io.jsonwebtoken.impl.Base64UrlCodec;

/**
 * Created by manish on 8/3/18.
 */

public class JwtUtils {

    public static String getUserId(String token) {

        String userId = "";
        String jwtToken = token;
        String[] base64UrlEncodedSegments = jwtToken.split("\\.");
        String base64UrlEncodedClaims = base64UrlEncodedSegments[1];
        String claims = base64UrlDecode(base64UrlEncodedClaims);

        try {

            JSONObject jsonObject = new JSONObject(claims);
            if(jsonObject.has("user_id")) {
               userId = String.valueOf(jsonObject.getInt("user_id"));
            }

            /*JsonObject jsonObject = new JsonObject(claims);
            if(jsonObject.has("user_id")) {
                jsonObject.get("user_id");
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userId;

    }

    public static String base64UrlDecode(String input) {
        String result = null;
        Base64UrlCodec decoder = new Base64UrlCodec();
        byte[] decodedBytes = decoder.decode(input);
        result = new String(decodedBytes);
        return result;
    }
}
