package amtv.iwant.amtvtv.util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusBuilder;

public class EventBusUtil
{
    private static EventBusUtil instance;
    private EventBus eventBus;
    public static EventBusUtil getInstance()
    {
        if (instance == null)
        {
            instance = new EventBusUtil();
        }
        return instance;
    }

    private EventBusUtil()
    {
        EventBus.getDefault();
        EventBusBuilder eventBusBuilder = EventBus.builder();
        eventBusBuilder.throwSubscriberException(false);
        eventBus = eventBusBuilder.build();
    }

    public EventBus getEventBus()
    {
        return eventBus;
    }
}
