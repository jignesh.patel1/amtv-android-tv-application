package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Child implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("childName")
    @Expose
    public String childName;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("birthDate")
    @Expose
    public String birthDate;
    @SerializedName("schoolName")
    @Expose
    public String schoolName;


}