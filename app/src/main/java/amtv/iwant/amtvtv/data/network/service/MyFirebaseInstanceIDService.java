package amtv.iwant.amtvtv.data.network.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";


    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("iwant");
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        updateDeviceTokenId();
    }

    private void updateDeviceTokenId() {

      /*  DataManager dataManager = new AppDataManager(MyFirebaseInstanceIDService.this);

        final String userId = dataManager.getSharedPreference().get(Constants.PREF_KEY_USER_ID, "");

        if(userId != null && userId.length() > 0) {
            DeviceTokenReq deviceTokenReq = new DeviceTokenReq();
            deviceTokenReq.setClientType(ClientType.MOBILE.toString());
            deviceTokenReq.setToken(FirebaseInstanceId.getInstance().getToken());
            deviceTokenReq.setUserId(userId);

            Observable<ResponseBody> observable = dataManager.getUserService().updateDeviceToken(deviceTokenReq);

            observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(new Consumer<ResponseBody>() {
                        @Override
                        public void accept(ResponseBody responseBody) throws Exception {
                            Timber.d("updated device token successfully.");
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Timber.e("Error on update device token.");
                        }
                    });
        }*/
    }

}
