package amtv.iwant.amtvtv.data.network.model.user;

/**
 * Created by manish on 8/3/18.
 */

public enum Gender {

    MALE,
    FEMALE,
    OTHER
}
