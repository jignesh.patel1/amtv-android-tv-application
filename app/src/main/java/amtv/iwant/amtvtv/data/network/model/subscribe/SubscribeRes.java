package amtv.iwant.amtvtv.data.network.model.subscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.Package;
import java.util.List;

public class SubscribeRes {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("subscriptionId")
    @Expose
    private Integer subscriptionId;
    @SerializedName("packages")
    @Expose
    private List<Package> packages = null;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("discountAmount")
    @Expose
    private Integer discountAmount;
    @SerializedName("dailySubscriptionCharge")
    @Expose
    private Integer dailySubscriptionCharge;
    @SerializedName("usedDays")
    @Expose
    private Integer usedDays;
    @SerializedName("remainingDays")
    @Expose
    private Integer remainingDays;
    @SerializedName("gracePeriodDays")
    @Expose
    private Integer gracePeriodDays;
    @SerializedName("isOnGracePeriod")
    @Expose
    private Boolean isOnGracePeriod;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("startAt")
    @Expose
    private String startAt;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    /*@SerializedName("status")
    @Expose
    private List<Status> status = null;*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getDailySubscriptionCharge() {
        return dailySubscriptionCharge;
    }

    public void setDailySubscriptionCharge(Integer dailySubscriptionCharge) {
        this.dailySubscriptionCharge = dailySubscriptionCharge;
    }

    public Integer getUsedDays() {
        return usedDays;
    }

    public void setUsedDays(Integer usedDays) {
        this.usedDays = usedDays;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public Integer getGracePeriodDays() {
        return gracePeriodDays;
    }

    public void setGracePeriodDays(Integer gracePeriodDays) {
        this.gracePeriodDays = gracePeriodDays;
    }

    public Boolean getIsOnGracePeriod() {
        return isOnGracePeriod;
    }

    public void setIsOnGracePeriod(Boolean isOnGracePeriod) {
        this.isOnGracePeriod = isOnGracePeriod;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /*public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }*/

}
