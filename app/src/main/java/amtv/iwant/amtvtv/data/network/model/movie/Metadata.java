package amtv.iwant.amtvtv.data.network.model.movie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

    @SerializedName("contentURL")
    @Expose
    private String contentURL;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("aspectRatio")
    @Expose
    private String aspectRatio;
    @SerializedName("streamingType")
    @Expose
    private String streamingType;

    public String getContentURL() {
        return contentURL;
    }

    public void setContentURL(String contentURL) {
        this.contentURL = contentURL;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(String aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public String getStreamingType() {
        return streamingType;
    }

    public void setStreamingType(String streamingType) {
        this.streamingType = streamingType;
    }

}