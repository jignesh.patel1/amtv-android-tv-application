package amtv.iwant.amtvtv.data.network.model.watch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatchReq {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("isLike")
    @Expose
    private Boolean isLike;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("mid")
    @Expose
    private String mid;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

}
