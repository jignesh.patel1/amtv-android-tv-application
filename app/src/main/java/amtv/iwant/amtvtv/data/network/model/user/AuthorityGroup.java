
package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AuthorityGroup {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("groupName")
    @Expose
    private String groupName;
    @SerializedName("businessId")
    @Expose
    private String businessId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("authorityGroupType")
    @Expose
    private String authorityGroupType;
    @SerializedName("licenceId")
    @Expose
    private String licenceId;
    @SerializedName("authorities")
    @Expose
    private List<Authority> authorities = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthorityGroupType() {
        return authorityGroupType;
    }

    public void setAuthorityGroupType(String authorityGroupType) {
        this.authorityGroupType = authorityGroupType;
    }

    public String getLicenceId() {
        return licenceId;
    }

    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

}