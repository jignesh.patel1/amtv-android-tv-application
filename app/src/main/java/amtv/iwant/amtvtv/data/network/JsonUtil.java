package amtv.iwant.amtvtv.data.network;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;


public class JsonUtil
{
    public static String dataKey = "Data";

    public static boolean hasProperty(JsonObject jsonObject, String property)
    {
        return jsonObject.has(property) && !jsonObject.get(property).isJsonNull();
    }

    public static String getStringByFieldName(String fieldName, JsonObject jsonObject)
    {
        String fieldValue = "";
        if (jsonObject.has(fieldName))
        {
            fieldValue = jsonObject.get(fieldName).getAsString();
        }
        return fieldValue;
    }

    //TODO::Experimental Generic Type Access At Runtime
    /* Do Not Change Name of Method it May Cause Problem as Its Accessing method name at runtime */
    public static <V> Map<String, V> getHashMap(JsonArray jsonArray, String keyFieldName)
    {
        Map<String, V> resultHashMap = new LinkedHashMap<>();
        Method method = getStaticMethod(JsonUtil.class, "getHashMap", JsonArray.class, String.class);
        Type[] types = method.getTypeParameters();
        Type genericTypeInfo = types[0];

        for (int jsonElementIndex = 0; jsonElementIndex < jsonArray.size(); jsonElementIndex++)
        {
            JsonObject jsonObject = jsonArray.get(jsonElementIndex).getAsJsonObject();
            String keyField = jsonObject.get(keyFieldName).getAsString();
            try
            {
                V value = GsonInterface.getInstance().getGson().fromJson(jsonObject, genericTypeInfo);
                resultHashMap.put(keyField, value);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        return resultHashMap;
    }

    static public Method getStaticMethod(Class<?> type, String methodName, Class<?>... params)
    {
        try
        {
            Method method = type.getDeclaredMethod(methodName, params);
            if ((method.getModifiers() & Modifier.STATIC) != 0)
            {
                return method;
            }
        }
        catch (NoSuchMethodException e)
        {
        }
        return null;
    }
}
