package amtv.iwant.amtvtv.data.network.model.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Business {

    @SerializedName("businessId")
    @Expose
    private Long businessId;
    @SerializedName("outLetName")
    @Expose
    private String outLetName;
    @SerializedName("businessTitle")
    @Expose
    private String businessTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("businessType")
    @Expose
    private BusinessType businessType;
    @SerializedName("delivery")
    @Expose
    private String delivery;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("contactDetails")
    @Expose
    private List<ContactDetail> contactDetails = null;
    @SerializedName("geoLocation")
    @Expose
    private GeoLocation geoLocation;
    @SerializedName("timeRanges")
    @Expose
    private List<TimeRange> timeRanges = null;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("cancellationPolicy")
    @Expose
    private String cancellationPolicy;
    @SerializedName("isHomeDeliveryAvailable")
    @Expose
    private Boolean isHomeDeliveryAvailable;
    @SerializedName("scheduledDelivery")
    @Expose
    private Boolean scheduledDelivery;
    @SerializedName("minimumOrderAmt")
    @Expose
    private Float minimumOrderAmt;
    @SerializedName("visitAvailable")
    @Expose
    private Boolean visitAvailable;
    @SerializedName("isPickupAvailable")
    @Expose
    private Boolean isPickupAvailable;

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getOutLetName() {
        return outLetName;
    }

    public void setOutLetName(String outLetName) {
        this.outLetName = outLetName;
    }

    public String getBusinessTitle() {
        return businessTitle;
    }

    public void setBusinessTitle(String businessTitle) {
        this.businessTitle = businessTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public List<TimeRange> getTimeRanges() {
        return timeRanges;
    }

    public void setTimeRanges(List<TimeRange> timeRanges) {
        this.timeRanges = timeRanges;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public Boolean getIsHomeDeliveryAvailable() {
        return isHomeDeliveryAvailable;
    }

    public void setIsHomeDeliveryAvailable(Boolean isHomeDeliveryAvailable) {
        this.isHomeDeliveryAvailable = isHomeDeliveryAvailable;
    }

    public Boolean getScheduledDelivery() {
        return scheduledDelivery;
    }

    public void setScheduledDelivery(Boolean scheduledDelivery) {
        this.scheduledDelivery = scheduledDelivery;
    }

    public Float getMinimumOrderAmt() {
        return minimumOrderAmt;
    }

    public void setMinimumOrderAmt(Float minimumOrderAmt) {
        this.minimumOrderAmt = minimumOrderAmt;
    }

    public Boolean getVisitAvailable() {
        return visitAvailable;
    }

    public void setVisitAvailable(Boolean visitAvailable) {
        this.visitAvailable = visitAvailable;
    }

    public Boolean getIsPickupAvailable() {
        return isPickupAvailable;
    }

    public void setIsPickupAvailable(Boolean isPickupAvailable) {
        this.isPickupAvailable = isPickupAvailable;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public Boolean getHomeDeliveryAvailable() {
        return isHomeDeliveryAvailable;
    }

    public void setHomeDeliveryAvailable(Boolean homeDeliveryAvailable) {
        isHomeDeliveryAvailable = homeDeliveryAvailable;
    }

    public Boolean getPickupAvailable() {
        return isPickupAvailable;
    }

    public void setPickupAvailable(Boolean pickupAvailable) {
        isPickupAvailable = pickupAvailable;
    }
}