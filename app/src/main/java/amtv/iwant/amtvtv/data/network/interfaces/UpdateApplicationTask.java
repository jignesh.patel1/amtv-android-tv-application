package amtv.iwant.amtvtv.data.network.interfaces;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import amtv.iwant.amtvtv.core.Constants;

/**
 * Created by ITPL09 on 3/11/2016.
 */
public class UpdateApplicationTask extends AsyncTask<Void, Void, String> {

    private Context context = null;
    private OnUpdateArrival onUpdateArrival = null;

    public UpdateApplicationTask(Context context) {
        this.context = context;
        onUpdateArrival = (OnUpdateArrival) context;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            PackageInfo pInfo = null;
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            URL url = new URL(Constants.PLAY_STORE_URL + pInfo.packageName);
            HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
            httpsConn.setDoInput(true);
            httpsConn.setDoOutput(true);

            StringBuffer sb = new StringBuffer();

            if (httpsConn.getResponseCode() == Constants.RESPONSE_CODE) {
                httpsConn.connect();

                BufferedReader in = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
                String decodedString;

                while ((decodedString = in.readLine()) != null) {
                    sb.append(decodedString);
                }

                in.close();
                httpsConn.disconnect();

                String dataAsString = sb.toString();
                String version = "softwareVersion" + '"' + ">";
                int startIndex = dataAsString.indexOf(version); // + version.length();

                if (startIndex != -1) {
                    startIndex += version.length();
                    dataAsString = dataAsString.substring(startIndex);
                    startIndex = dataAsString.indexOf("</");
                    String playStoreVersion = dataAsString.substring(0, startIndex).trim();
                    String localVersion = pInfo.versionName.trim();

                    if (!localVersion.equalsIgnoreCase(playStoreVersion)) {

                        int playstoreBuildNumber = Integer.parseInt((playStoreVersion.substring(playStoreVersion.lastIndexOf(".") + 1)));
                        int localBuildNumber = Integer.parseInt((localVersion.substring(localVersion.lastIndexOf(".") + 1)));

                        if (localBuildNumber < playstoreBuildNumber)
                            return "1";
                        else
                            return "0";
                    } else {
                        return "0";
                    }
                } else {
                    return "0";
                }
            } else {
                return Constants.SERVER_NOT_READY;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "0";
        }
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        if (onUpdateArrival != null) {
            if (response.equals("1")) {
                onUpdateArrival.updateFound(true);
            } else if (response.equals("0")) {
                onUpdateArrival.updateFound(false);
            }
        }
    }
}