package amtv.iwant.amtvtv.data.network.deserializer.base;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import amtv.iwant.amtvtv.data.network.JsonUtil;
import amtv.iwant.amtvtv.data.network.model.base.ApiError;

public class ApiErrorDs implements JsonDeserializer<ApiError>
{
    @Override
    public ApiError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
    {
        ApiError error = new ApiError();
        JsonObject jsonObject = json.getAsJsonObject();

        /*{
            "timestamp" : "2017-03-14T14:14:05.131+05:30",
            "status" : "NOT_FOUND",
            "code" : "IW-BUSRUSR005",
            "message" : "Cannot find user for given id null",
            "fieldErrors" : [ ],
            "cause" : null

        }*/

        if (JsonUtil.hasProperty(jsonObject, "timestamp"))
        {
            String key = jsonObject.get("timestamp").getAsString();
            error.setTimeStamp(key);
        }

        if (JsonUtil.hasProperty(jsonObject, "status"))
        {
            String key = jsonObject.get("status").getAsString();
            error.setStatus(key);
        }

        if (JsonUtil.hasProperty(jsonObject, "code"))
        {
            String key = jsonObject.get("code").getAsString();
            error.setCode(key);
        }


        if (JsonUtil.hasProperty(jsonObject, "message"))
        {
            String key = jsonObject.get("message").getAsString();
            error.setMessage(key);
        }

        if (JsonUtil.hasProperty(jsonObject, "error"))
        {
            String key = jsonObject.get("error").getAsString();
            error.setStatus(key);
        }

        if (JsonUtil.hasProperty(jsonObject, "error_description"))
        {
            String key = jsonObject.get("error_description").getAsString();
            error.setMessage(key);
        }

        return error;
    }
}
