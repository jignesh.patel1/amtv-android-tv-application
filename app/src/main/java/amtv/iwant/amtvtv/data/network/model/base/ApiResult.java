package amtv.iwant.amtvtv.data.network.model.base;


public class ApiResult
{
    private boolean isSuccess = false;

    public boolean isSuccess()
    {
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess)
    {
        this.isSuccess = isSuccess;
    }
}
