package amtv.iwant.amtvtv.data.network.interfaces;

/**
 * Created by Kalpen on 11-03-2016.
 */
public interface OnUpdateArrival {
    public void updateFound(boolean update);
}