package amtv.iwant.amtvtv.data.network.model.notification;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by manish on 15/12/17.
 */

public interface INotificationRepository {

    interface OnGetAllNotificationCallback {
        void onSuccess(RealmResults<NotificationRes> notificationResults);
        void onError(String message);
    }

    interface OnDeleteNotificationCallback {
        void onSuccess(String message);
        void onError(String message);
    }

    interface OnDeleteAllNotificationCallback {
        void onSuccess(String message);
        void onError(String message);
    }

    public void addNotificationAsync(NotificationRes notificationRes);

    public void addNotification(NotificationRes notificationRes);

    public void getAllNotification(Realm realm, OnGetAllNotificationCallback onGetAllNotificationCallback);

    public void deleteNotificationByPosition(Realm realm, int position, OnDeleteNotificationCallback onDeleteNotificationCallback);

    public void deleteAllNotification(Realm realm, OnDeleteAllNotificationCallback onDeleteNotificationCallback);

    public void deleteNotificationById(Realm realm, final String id, OnDeleteNotificationCallback callback);
}
