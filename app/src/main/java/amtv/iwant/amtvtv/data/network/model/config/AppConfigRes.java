package amtv.iwant.amtvtv.data.network.model.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amtv.iwant.amtvtv.data.network.model.base.BaseRes;

/**
 * Created by manish on 9/11/17.
 */

public class AppConfigRes extends BaseRes {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("internationalization")
    @Expose
    private Internationalization internationalization;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("defaultCollectionsCount")
    @Expose
    private Integer defaultCollectionsCount;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("contentBackgroundImageUrl")
    @Expose
    private String contentBackgroundImageUrl;
    @SerializedName("playerBackgroundImageUrl")
    @Expose
    private String playerBackgroundImageUrl;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("otpConfigs")
    @Expose
    private List<OtpConfig> otpConfigs = null;
    @SerializedName("isGlobalOTPExpiryTime")
    @Expose
    private Boolean isGlobalOTPExpiryTime;
    @SerializedName("isOnlinePayment")
    @Expose
    private Boolean isOnlinePayment;
    @SerializedName("contentBackgroundImageColor")
    @Expose
    private String contentBackgroundImageColor;
    @SerializedName("playerBackgroundImageUrlMobile")
    @Expose
    private String playerBackgroundImageUrlMobile;
    @SerializedName("contentBackgroundImageUrlMobile")
    @Expose
    private String contentBackgroundImageUrlMobile;

    public String getContentBackgroundImageUrlMobile() {
        return contentBackgroundImageUrlMobile;
    }

    public void setContentBackgroundImageUrlMobile(String contentBackgroundImageUrlMobile) {
        this.contentBackgroundImageUrlMobile = contentBackgroundImageUrlMobile;
    }

    public String getPlayerBackgroundImageUrlMobile() {
        return playerBackgroundImageUrlMobile;
    }

    public void setPlayerBackgroundImageUrlMobile(String playerBackgroundImageUrlMobile) {
        this.playerBackgroundImageUrlMobile = playerBackgroundImageUrlMobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Internationalization getInternationalization() {
        return internationalization;
    }

    public void setInternationalization(Internationalization internationalization) {
        this.internationalization = internationalization;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Integer getDefaultCollectionsCount() {
        return defaultCollectionsCount;
    }

    public void setDefaultCollectionsCount(Integer defaultCollectionsCount) {
        this.defaultCollectionsCount = defaultCollectionsCount;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getContentBackgroundImageUrl() {
        return contentBackgroundImageUrl;
    }

    public void setContentBackgroundImageUrl(String contentBackgroundImageUrl) {
        this.contentBackgroundImageUrl = contentBackgroundImageUrl;
    }

    public String getPlayerBackgroundImageUrl() {
        return playerBackgroundImageUrl;
    }

    public void setPlayerBackgroundImageUrl(String playerBackgroundImageUrl) {
        this.playerBackgroundImageUrl = playerBackgroundImageUrl;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public List<OtpConfig> getOtpConfigs() {
        return otpConfigs;
    }

    public void setOtpConfigs(List<OtpConfig> otpConfigs) {
        this.otpConfigs = otpConfigs;
    }

    public Boolean getIsGlobalOTPExpiryTime() {
        return isGlobalOTPExpiryTime;
    }

    public void setIsGlobalOTPExpiryTime(Boolean isGlobalOTPExpiryTime) {
        this.isGlobalOTPExpiryTime = isGlobalOTPExpiryTime;
    }

    public Boolean getIsOnlinePayment() {
        return isOnlinePayment;
    }

    public void setIsOnlinePayment(Boolean isOnlinePayment) {
        this.isOnlinePayment = isOnlinePayment;
    }

    public String getContentBackgroundImageColor() {
        return contentBackgroundImageColor;
    }

    public void setContentBackgroundImageColor(String contentBackgroundImageColor) {
        this.contentBackgroundImageColor = contentBackgroundImageColor;
    }

}