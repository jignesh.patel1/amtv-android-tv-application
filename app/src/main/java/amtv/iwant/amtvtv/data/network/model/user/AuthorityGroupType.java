package amtv.iwant.amtvtv.data.network.model.user;

/**
 * Created by manish on 9/11/17.
 */

public enum AuthorityGroupType {

    BUSINESS_ADMIN,
    BUSINESS_STAFF,
    BUSINESS_CUSTOMER,
    IWANT_ADMIN
}
