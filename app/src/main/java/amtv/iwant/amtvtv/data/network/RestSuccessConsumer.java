package amtv.iwant.amtvtv.data.network;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

/**
 * Created by manish on 8/11/17.
 */

public abstract class RestSuccessConsumer<T> implements Consumer<T> {

    @Override
    public void accept(@NonNull T t) throws Exception {
        success(t);
    }

    public abstract void success(@NonNull T t);

}
