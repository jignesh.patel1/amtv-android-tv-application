package amtv.iwant.amtvtv.data.network.model.collection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Collection {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("shortTitle")
    @Expose
    private String shortTitle;
    @SerializedName("imageURL")
    @Expose
    private String imageURL;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("channels")
    @Expose
    private List<String> channels = null;
    @SerializedName("contents")
    @Expose
    private List<Content> contents = null;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("collectionTemplate")
    @Expose
    private String collectionTemplate;
    @SerializedName("coverImageURLs")
    @Expose
    private List<String> coverImageURLs = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<String> getChannels() {
        return channels;
    }

    public void setChannels(List<String> channels) {
        this.channels = channels;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getCollectionTemplate() {
        return collectionTemplate;
    }

    public void setCollectionTemplate(String collectionTemplate) {
        this.collectionTemplate = collectionTemplate;
    }

    public List<String> getCoverImageURLs() {
        return coverImageURLs;
    }

    public void setCoverImageURLs(List<String> coverImageURLs) {
        this.coverImageURLs = coverImageURLs;
    }
}
