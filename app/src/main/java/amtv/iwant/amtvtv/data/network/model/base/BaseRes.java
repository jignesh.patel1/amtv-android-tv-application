package amtv.iwant.amtvtv.data.network.model.base;

import com.google.gson.JsonElement;

public class BaseRes
{
    private ApiResult apiResult = new ApiResult();

    public ApiResult getApiResult()
    {
        return apiResult;
    }

    public void setApiResult(ApiResult apiResult)
    {
        this.apiResult = apiResult;
    }

    public void decodeResult(JsonElement jsonElement)
    {

    }

}
