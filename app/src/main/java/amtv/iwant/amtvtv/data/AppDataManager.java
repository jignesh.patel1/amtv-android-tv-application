/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package amtv.iwant.amtvtv.data;

import android.content.Context;

import amtv.iwant.amtvtv.R;
import amtv.iwant.amtvtv.data.database.RealmManager;
import amtv.iwant.amtvtv.data.network.WebService;
import amtv.iwant.amtvtv.data.network.service.ProductService;
import amtv.iwant.amtvtv.data.network.service.UserService;
import amtv.iwant.amtvtv.data.prefs.AppSharedPrefsHelper;
import amtv.iwant.amtvtv.data.prefs.Prefs;
import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by janisharali on 27/01/17.
 */

public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";
    private final Context mContext;

    public AppDataManager(Context context) {
        mContext = context;
    }


    @Override
    public void updateApiHeader(Long userId, String accessToken) {

    }

    @Override
    public void setUserAsLoggedOut() {

    }

    @Override
    public Observable<Boolean> seedDatabaseQuestions() {
        return null;
    }

    @Override
    public Observable<Boolean> seedDatabaseOptions() {
        return null;
    }

    @Override
    public void updateUserInfo(String accessToken, Long userId, LoggedInMode loggedInMode, String userName, String email, String profilePicPath) {

    }

    @Override
    public UserService getUserService() {
        return WebService.getInstance().getUserService();
    }

    @Override
    public ProductService getProductService() {
        return WebService.getInstance().getProductService();
    }

    @Override
    public void setBaseUrl() {
        WebService.getInstance().initUser(mContext.getString(R.string.app_base_url));
        WebService.getInstance().initProduct(mContext.getString(R.string.app_base_url));
    }

    @Override
    public String getAppCode() {
        return mContext.getString(R.string.app_code);
    }

    @Override
    public void setRealmManager() {
        RealmManager.initializeRealmConfig();
    }

    @Override
    public Realm openRealm() {
        RealmManager.incrementCount();
        return RealmManager.getRealm();
    }

    @Override
    public void closeRealm() {
        RealmManager.decrementCount();
    }

    @Override
    public AppSharedPrefsHelper getSharedPreference() {
        AppSharedPrefsHelper appSharedPrefsHelper = new AppSharedPrefsHelper(Prefs.getPreferences());
        return appSharedPrefsHelper;
    }


}
