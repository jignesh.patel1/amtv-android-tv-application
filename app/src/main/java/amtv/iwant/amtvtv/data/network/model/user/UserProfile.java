package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserProfile implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("birthDate")
    @Expose
    public String birthDate;
    @SerializedName("bloodGroup")
    @Expose
    public String bloodGroup;
    @SerializedName("weight")
    @Expose
    public Integer weight;
    @SerializedName("occupation")
    @Expose
    public String occupation;
    @SerializedName("spouseName")
    @Expose
    public String spouseName;
    @SerializedName("anniversaryDate")
    @Expose
    public String anniversaryDate;
    @SerializedName("spouseBirthDate")
    @Expose
    public String spouseBirthDate;
    @SerializedName("maritalStatus")
    @Expose
    public String maritalStatus;
    @SerializedName("designation")
    @Expose
    public String designation;
    @SerializedName("alterNetMobileNo")
    @Expose
    public Integer alterNetMobileNo;
    @SerializedName("faxNo")
    @Expose
    public Integer faxNo;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("addresses")
    @Expose
    public List<Address> addresses = null;
    @SerializedName("children")
    @Expose
    public List<Child> children = null;

    public String getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public Integer getWeight() {
        return weight;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public String getAnniversaryDate() {
        return anniversaryDate;
    }

    public String getSpouseBirthDate() {
        return spouseBirthDate;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public String getDesignation() {
        return designation;
    }

    public Integer getAlterNetMobileNo() {
        return alterNetMobileNo;
    }

    public Integer getFaxNo() {
        return faxNo;
    }

    public String getWebsite() {
        return website;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}