package amtv.iwant.amtvtv.data.network.model.notification;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by manish on 15/12/17.
 */

public class NotificationRepository implements INotificationRepository {

    @Override
    public void addNotificationAsync(final NotificationRes notificationRes) {

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                NotificationRes notificationRealm = realm.createObject(NotificationRes.class);
                notificationRealm.setId(UUID.randomUUID().toString());
                notificationRealm.setTitle(notificationRes.getTitle());
                notificationRealm.setCreatedAt(notificationRes.getCreatedAt());
                notificationRealm.setDescription(notificationRes.getDescription());
                notificationRealm.setOrderDate(notificationRes.getOrderDate());
                notificationRealm.setOrderId(notificationRes.getOrderId());
                notificationRealm.setNotificationType(notificationRes.getNotificationType());
                notificationRealm.setOrderNumber(notificationRes.getOrderNumber());
                realm.insert(notificationRealm);
            }
        });
        realm.close();
    }

    @Override
    public void addNotification(final NotificationRes notificationRes) {

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                NotificationRes notificationRealm = realm.createObject(NotificationRes.class);
                notificationRealm.setId(UUID.randomUUID().toString());
                notificationRealm.setTitle(notificationRes.getTitle());
                notificationRealm.setCreatedAt(notificationRes.getCreatedAt());
                notificationRealm.setDescription(notificationRes.getDescription());
                notificationRealm.setOrderDate(notificationRes.getOrderDate());
                notificationRealm.setOrderId(notificationRes.getOrderId());
                notificationRealm.setNotificationType(notificationRes.getNotificationType());
                notificationRealm.setOrderNumber(notificationRes.getOrderNumber());
                realm.insert(notificationRealm);
            }
        });
        realm.close();
    }

    @Override
    public void getAllNotification(Realm realm, OnGetAllNotificationCallback callback) {
        RealmResults<NotificationRes> results = realm.where(NotificationRes.class).findAll();
        if (callback != null)
            callback.onSuccess(results);
    }

    @Override
    public void deleteNotificationByPosition(Realm realm, final int position, OnDeleteNotificationCallback callback) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmQuery<NotificationRes> query = realm.where(NotificationRes.class);
                RealmResults<NotificationRes> results = query.findAll();
                results.deleteFromRealm(position);

            }
        });

        if (callback != null)
            callback.onSuccess("Notification remove successfully.");
    }

    @Override
    public void deleteNotificationById(Realm realm, final String id, OnDeleteNotificationCallback callback) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                NotificationRes notificationRes = realm.where(NotificationRes.class).equalTo("id", id).findFirst();
                notificationRes.deleteFromRealm();

            }
        });

        if (callback != null)
            callback.onSuccess("Notification remove successfully.");
    }

    @Override
    public void deleteAllNotification(Realm realm, OnDeleteAllNotificationCallback callback) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmQuery<NotificationRes> query = realm.where(NotificationRes.class);
                RealmResults<NotificationRes> results = query.findAll();
                results.deleteAllFromRealm();
            }
        });

        if (callback != null)
            callback.onSuccess("Notification remove successfully.");
    }
}
