package amtv.iwant.amtvtv.data.network.model.banner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("webImageURL")
    @Expose
    private String webImageURL;
    @SerializedName("mobileImageURL")
    @Expose
    private String mobileImageURL;
    @SerializedName("target")
    @Expose
    private Object target;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("targetType")
    @Expose
    private String targetType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebImageURL() {
        return webImageURL;
    }

    public void setWebImageURL(String webImageURL) {
        this.webImageURL = webImageURL;
    }

    public String getMobileImageURL() {
        return mobileImageURL;
    }

    public void setMobileImageURL(String mobileImageURL) {
        this.mobileImageURL = mobileImageURL;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}
