package amtv.iwant.amtvtv.data.network.interceptor;

import android.text.TextUtils;

import java.io.IOException;

import amtv.iwant.amtvtv.core.AmtvApp;
import amtv.iwant.amtvtv.core.Constants;
import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.networkexception.NetworkNotAvailableException;
import amtv.iwant.amtvtv.util.NetworkUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor
{
    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();
        if (!NetworkUtils.isNetworkConnected(AmtvApp.getContext()))
        {
            throw new NetworkNotAvailableException();
        }
        /* Build Request with headers */
        Request.Builder requestBuilder = request.newBuilder()
                .addHeader("Content-Type", "application/json");

        DataManager dataManager = new AppDataManager(AmtvApp.getContext());


        if (!TextUtils.isEmpty(dataManager.getSharedPreference().get(Constants.PREF_KEY_OAUTH_TOKEN, "")))
        {
            requestBuilder = requestBuilder.addHeader("Authorization", ""
                    + "Bearer" + " "
                    + dataManager.getSharedPreference().get(Constants.PREF_KEY_OAUTH_TOKEN, ""));
        }


        request = requestBuilder.build();
        return chain.proceed(request);
    }
}