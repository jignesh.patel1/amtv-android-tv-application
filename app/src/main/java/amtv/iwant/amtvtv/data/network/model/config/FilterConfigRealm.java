package amtv.iwant.amtvtv.data.network.model.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class FilterConfigRealm extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("appName")
    @Expose
    private String appName;
    @SerializedName("businessConfigurationId")
    @Expose
    private Integer businessConfigurationId;
    @SerializedName("parameters")
    @Expose
    private RealmList<ParameterRealm> parameters = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getBusinessConfigurationId() {
        return businessConfigurationId;
    }

    public void setBusinessConfigurationId(Integer businessConfigurationId) {
        this.businessConfigurationId = businessConfigurationId;
    }

    public RealmList<ParameterRealm> getParameters() {
        return parameters;
    }

    public void setParameters(RealmList<ParameterRealm> parameters) {
        this.parameters = parameters;
    }

}