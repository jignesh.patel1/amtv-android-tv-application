package amtv.iwant.amtvtv.data.network.model.authoriz;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqAuthoriz {
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("content")
    @Expose
    private String content;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
