package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amtv.iwant.amtvtv.data.network.model.base.BaseReq;

/**
 * Created by manish on 10/11/17.
 */

public class OauthTokenReq extends BaseReq {

    @SerializedName("grant_type")
    @Expose
    private String grantType;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
