package amtv.iwant.amtvtv.data.network.interceptor;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class PicassoRequestInterceptor implements Interceptor
{
    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();

        /* Build Request with headers */
        Request.Builder requestBuilder = request.newBuilder()
                /*.addHeader("AppAccessKey", AppSettings.getInstance().getAccessKey())*/
                .addHeader("Accept", "image/webp,image/*,*/*;q=0.8")
                /*.addHeader("AppPrivateKey", AppSettings.getInstance().getPrivateKey())*/;

        /*if (!AppSettings.getInstance().getSessionKey().trim().equals(""))
        {
            requestBuilder = requestBuilder.addHeader("SessionToken", AppSettings.getInstance().getSessionKey());
        }*/
        request = requestBuilder.build();
        return chain.proceed(request);
    }
}