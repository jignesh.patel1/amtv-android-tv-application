package amtv.iwant.amtvtv.data.network.model.movie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingsMetadata {

    @SerializedName("ratingOneCount")
    @Expose
    private Integer ratingOneCount;
    @SerializedName("ratingTwoCount")
    @Expose
    private Integer ratingTwoCount;
    @SerializedName("ratingThreeCount")
    @Expose
    private Integer ratingThreeCount;
    @SerializedName("ratingFourCount")
    @Expose
    private Integer ratingFourCount;
    @SerializedName("ratingFiveCount")
    @Expose
    private Integer ratingFiveCount;
    @SerializedName("totalRatingsCount")
    @Expose
    private Integer totalRatingsCount;
    @SerializedName("averageRatings")
    @Expose
    private Integer averageRatings;

    public Integer getRatingOneCount() {
        return ratingOneCount;
    }

    public void setRatingOneCount(Integer ratingOneCount) {
        this.ratingOneCount = ratingOneCount;
    }

    public Integer getRatingTwoCount() {
        return ratingTwoCount;
    }

    public void setRatingTwoCount(Integer ratingTwoCount) {
        this.ratingTwoCount = ratingTwoCount;
    }

    public Integer getRatingThreeCount() {
        return ratingThreeCount;
    }

    public void setRatingThreeCount(Integer ratingThreeCount) {
        this.ratingThreeCount = ratingThreeCount;
    }

    public Integer getRatingFourCount() {
        return ratingFourCount;
    }

    public void setRatingFourCount(Integer ratingFourCount) {
        this.ratingFourCount = ratingFourCount;
    }

    public Integer getRatingFiveCount() {
        return ratingFiveCount;
    }

    public void setRatingFiveCount(Integer ratingFiveCount) {
        this.ratingFiveCount = ratingFiveCount;
    }

    public Integer getTotalRatingsCount() {
        return totalRatingsCount;
    }

    public void setTotalRatingsCount(Integer totalRatingsCount) {
        this.totalRatingsCount = totalRatingsCount;
    }

    public Integer getAverageRatings() {
        return averageRatings;
    }

    public void setAverageRatings(Integer averageRatings) {
        this.averageRatings = averageRatings;
    }

}