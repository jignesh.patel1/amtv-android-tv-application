package amtv.iwant.amtvtv.data.network.event;

public class RestEvent
{
    private String tag = "";
    private RestError restError = new RestError();

    public RestError getRestError()
    {
        return restError;
    }

    public void setRestError(RestError restError)
    {
        this.restError = restError;
    }

    public boolean isError()
    {
        if (restError.getRestErrorType() != RestErrorType.NONE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }
}
