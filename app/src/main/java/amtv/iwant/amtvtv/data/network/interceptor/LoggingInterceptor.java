package amtv.iwant.amtvtv.data.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class LoggingInterceptor implements Interceptor
{
    boolean enabledLogs = true;

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();
        if (enabledLogs)
        {
            long t1 = System.nanoTime();
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            String bodyString = response.body().string();

            return response.newBuilder().body(ResponseBody.create(response.body().contentType(), bodyString)).build();
        }
        else
        {
            return chain.proceed(request);
        }
    }

    public static String bodyToString(final Request request)
    {
        try
        {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        }
        catch (final IOException e)
        {
            return "did not work";
        }
    }
}