package amtv.iwant.amtvtv.data.network.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ApiError implements Serializable
{
    private String timeStamp = "";
    private String status = "";
    private String code = "";
    private String message = "";
    private String detail = "";

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @SerializedName("fieldErrors")
    @Expose
    private List<FieldError> fieldErrors = new ArrayList<>();

    public String getTimeStamp()
    {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public List<FieldError> getFieldError()
    {
        return fieldErrors;
    }

    public void setFieldError(List<FieldError> fieldError)
    {
        this.fieldErrors = fieldError;
    }
}
