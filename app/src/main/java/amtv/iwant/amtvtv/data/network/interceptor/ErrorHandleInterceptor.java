package amtv.iwant.amtvtv.data.network.interceptor;

import android.app.Activity;


import java.io.IOException;

import amtv.iwant.amtvtv.core.AmtvApp;
import amtv.iwant.amtvtv.core.Constants;
import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.HttpClientController;
import amtv.iwant.amtvtv.data.network.model.profile.GetUserByIdResRealm;
import amtv.iwant.amtvtv.data.network.model.user.OauthTokenReq;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ErrorHandleInterceptor implements Interceptor {

	@Override
	public Response intercept(final Chain chain) throws IOException {
		Response response = chain.proceed(chain.request());
		if (response.code() == 401) { //if unauthorized
			OkHttpClient okHttpClient = HttpClientController.getInstance().getHttpClient();
			CompositeDisposable compositeDisposable = new CompositeDisposable();
			final OauthTokenReq oauthTokenReq = new OauthTokenReq();
			oauthTokenReq.setGrantType("");
			oauthTokenReq.setRefreshToken("");
			synchronized (okHttpClient) {

				/*compositeDisposable.add(WebService.getInstance().getUserService()
						.oauthToken(oauthTokenReq)
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(new RestSuccessConsumer<OauthTokenRes>() {
							@Override
							public void success(@NonNull OauthTokenRes oauthTokenRes) {
								if (!TextUtils.isEmpty(oauthTokenRes.getAccessToken())) {
									Request request = chain.request();
									Request.Builder builder = request.newBuilder();
									builder = builder.addHeader("Authorization", "" + "Bearer" + " " + oauthTokenRes.getAccessToken());
									request = builder.build();
									try {
										chain.proceed(request);
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							}
						}, new RestErrorConsumer() {
							@Override
							public void error(RestError restError) {
								//logout();
							}
						}));
*/
			}
		}

		return response;
	}
	
	private void setAuthHeader(Request.Builder builder, String token) {
		if (token != null)
			builder.header("Authorization", String.format("Bearer %s", token));
	}

	private void logout() {

		Activity currentActivity = AmtvApp.getContext().getCurrentActivity();

		/*if(currentActivity instanceof UserActivity) {
			return;
		}*/

		DataManager dataManager = null;
		dataManager = new AppDataManager(AmtvApp.getContext());

		dataManager.openRealm().executeTransaction(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.delete(GetUserByIdResRealm.class);
			}
		});

		dataManager.closeRealm();

		dataManager.getSharedPreference().put(Constants.PREF_KEY_IS_LOGGED_IN, false);
		dataManager.getSharedPreference().put(Constants.PREF_KEY_OAUTH_TOKEN, "");
		dataManager.getSharedPreference().put(Constants.PREF_KEY_USER_ID, "");

		/*Intent i = new Intent(AmtvApp.getContext(), UserActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		AmtvApp.getContext().startActivity(i);*/
	}
}