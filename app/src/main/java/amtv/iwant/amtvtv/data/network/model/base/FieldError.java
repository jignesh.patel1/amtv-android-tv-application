package amtv.iwant.amtvtv.data.network.model.base;

import java.io.Serializable;

/**
 * Created by manish on 5/4/17.
 */

public class FieldError implements Serializable {

    String field;
    String message;

    public String getField()
    {
        return field;
    }

    public String getMessage()
    {
        return message;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
