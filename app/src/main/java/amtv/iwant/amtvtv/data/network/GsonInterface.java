package amtv.iwant.amtvtv.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import amtv.iwant.amtvtv.data.network.deserializer.base.ApiErrorDs;
import amtv.iwant.amtvtv.data.network.deserializer.base.ApiResultDs;
import amtv.iwant.amtvtv.data.network.model.base.ApiError;
import amtv.iwant.amtvtv.data.network.model.base.ApiResult;

public class GsonInterface
{
    private static GsonInterface instance;
    private final Gson gson;

    public static GsonInterface getInstance()
    {
        if (instance == null)
        {
            instance = new GsonInterface();
        }
        return instance;
    }

    private GsonInterface()
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        registerTypes(gsonBuilder);
        gsonBuilder = gsonBuilder.serializeNulls();
        gson = gsonBuilder
                .create();
    }

    public Gson getGson()
    {
        return gson;
    }

    public void registerTypes(GsonBuilder gsonBuilder)
    {
        gsonBuilder.registerTypeAdapter(ApiResult.class, new ApiResultDs());
        gsonBuilder.registerTypeAdapter(ApiError.class, new ApiErrorDs());

//        gsonBuilder.registerTypeAdapter(LoginRes.class, new LoginRes());

    }
}