package amtv.iwant.amtvtv.data.network.service;


import java.util.List;
import java.util.Map;

import amtv.iwant.amtvtv.data.network.model.collection.Collection;
import amtv.iwant.amtvtv.data.network.model.collection.CollectionRes;
import amtv.iwant.amtvtv.data.network.model.movie.MovieRes;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ProductService
{


    /*@GET("amtvservices/amtv-services/api/v1/banners/sp/")
    Observable<BannerRes> bannerList(@QueryMap Map<String, String> map);

    @GET("amtvservices/amtv-services/api/v1/contents/cp/getFeaturedContents")
    Observable<List<FeaturedRes>> featuredcList(@QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/marketplaces/getMarketplaceByMid/1")
    Observable<AppConfigRes> configureDetail(@QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/categories")
    Observable<CategoryRes> categoryList(@QueryMap Map<String, String> queryMap);

    //http://gateway.amtv.id/amtvservices/amtv-services/api/v1/categories/getByContentSubType?mid=1&page=1&limit=-1&contentSubType=Movie
    @GET("amtvservices/amtv-services/api/v1/categories/getByContentSubType")
    Observable<List<CategoryFrag>> categoryByTypeList(@QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/contents/cp/{movieId}")
    Observable<DetailRes> movieDetail(@Path("movieId") String movieId, @QueryMap Map<String, String> queryMap);
*/

    @GET("amtvservices/amtv-services/api/v1/contents/cp")
    Observable<MovieRes> movieList(@QueryMap Map<String, String> queryMap);


    @GET("amtvservices/amtv-services/api/v1/contents/cp")
    Observable<MovieRes> tvShowList(@QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/contents/cp")
    Observable<MovieRes> tvLiveTvList(@QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/collections/cp/{collectionId}")
    Observable<Collection> collectionListAll(@Path("collectionId") String collectionId, @QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/collections/cp/")
    Observable<CollectionRes> collectionList(@QueryMap Map<String, String> queryMap);

  /*  @POST("amtvservices/amtv-services/api/v1/contents/cp/isAuthorized")
    Observable<ResAuthoriz> isAuthorized(@Body ReqAuthoriz reqAuthoriz);

    @POST("amtvservices/amtv-services/api/v1/userlikes")
    Observable<WatchRes> watchVideo(@Body WatchReq watchReq);

    @POST("iwantuua/api/sendOTP")
    Observable<SendOtpRes> sendOtp(@Body SendOtpReq sendOtpReq);

    @POST("iwantuua/api/verifyOTP")
    Observable<VerifyOtpRes> verifyOtp(@Body VerifyOtpReq verifyOtpReq);

   // @GET("iwantuua/api/city/countryId/{countryId}")
   // Observable<List<City>> cityList(@QueryMap Map<String, String> queryMap, String countryId);

    @GET("iwantuua/api/city/countryId/{countryId}")
    Observable<List<City>> cityList(@Path("countryId") String countryId, @QueryMap Map<String, String> queryMap);

    @GET("amtvservices/amtv-services/api/v1/package/getAllPackageWeb")
    Observable<List<SubscribePlanRes>> callSubscribePlan(@QueryMap Map<String, String> queryMap);

    //amtvservices/amtv-services/api/v1/userlikes?mid=1&page=1&limit=-1&user=68&contentSubType=Series
    @GET("amtvservices/amtv-services/api/v1/userlikes")
    Observable<MovieRes> collectionFavList(@QueryMap Map<String, String> queryMap);

    //amtvservices/amtv-services/api/v1/contents/cp?mid=1&page=1&limit=15&contentSubType=Movie&search=Vod

    @GET("amtvservices/amtv-services/api/v1/contents/cp")
    Observable<MovieRes> searchList(@QueryMap Map<String, String> queryMap);

    @PUT("amtvservices/amtv-services/api/v1/subscription/{subscribeId}")
    Observable<SubscribeRes> subscribePackage(@Path("subscribeId") String subscribeId, @Body SubscibeReq subscibeReq);

    @GET("amtvservices/amtv-services/api/v1/subscription/userId/{userId}?isDefault=false")
    Observable<SubscribeDetailRes> callSubscribeDetail(@Path("userId") String userId);

    @GET("amtvservices/amtv-services/api/v1/subscription/userId/{userId}?isDefault=false")
    Observable<MySubscribeRes> mySubscribe(@Path("userId") String userId);

    //amtvservices/amtv-services/api/v1/collections/cp/5ba4cfd6f1706a03aa5ee860?user=199
    */

}

