package amtv.iwant.amtvtv.data.network.model.user;

/**
 * Created by manish on 9/11/17.
 */

public enum ClientType {

    MOBILE,
    WEB,
    ANDROID
}
