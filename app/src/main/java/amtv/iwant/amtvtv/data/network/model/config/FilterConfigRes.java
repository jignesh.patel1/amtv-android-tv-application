package amtv.iwant.amtvtv.data.network.model.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amtv.iwant.amtvtv.data.network.model.base.BaseRes;

public class FilterConfigRes extends BaseRes {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("appName")
    @Expose
    private String appName;
    @SerializedName("businessConfigurationId")
    @Expose
    private Integer businessConfigurationId;
    @SerializedName("parameters")
    @Expose
    private List<Parameter> parameters = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getBusinessConfigurationId() {
        return businessConfigurationId;
    }

    public void setBusinessConfigurationId(Integer businessConfigurationId) {
        this.businessConfigurationId = businessConfigurationId;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

}