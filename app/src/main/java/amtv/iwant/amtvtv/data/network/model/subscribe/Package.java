package amtv.iwant.amtvtv.data.network.model.subscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Package {

    @SerializedName("packageRef")
    @Expose
    private String packageRef;
    @SerializedName("subscriptionType")
    @Expose
    private String subscriptionType;

    public String getPackageRef() {
        return packageRef;
    }

    public void setPackageRef(String packageRef) {
        this.packageRef = packageRef;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

}