package amtv.iwant.amtvtv.data.repository;

import java.util.HashMap;
import java.util.Map;

import amtv.iwant.amtvtv.core.Constants;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.model.collection.Collection;
import amtv.iwant.amtvtv.data.network.model.collection.CollectionRes;
import amtv.iwant.amtvtv.data.network.model.movie.MovieRes;
import io.reactivex.Observable;

/**
 * Created by manish on 1/2/18.
 */

public class BaseRepository {




 /*   public Observable<BannerRes> bannerList(DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        Observable<BannerRes> observable = dataManager.getProductService().bannerList(queryMap);
        return observable;
    }


    public Observable<List<FeaturedRes>> featuredList(DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("mid", Constants.MARKET_PLACE_ID);
        Observable<List<FeaturedRes>> observable = dataManager.getProductService().featuredcList(queryMap);
        return observable;
    }


    public Observable<AppConfigRes> configureDetail(DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        Observable<AppConfigRes> observable = dataManager.getProductService().configureDetail(queryMap);
        return observable;
    }*/

    public Observable<Collection> collectionListAll(String userId, String collectionId, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("user", userId);
        Observable<Collection> observable = dataManager.getProductService().collectionListAll(collectionId,queryMap);
        return observable;
    }

    public Observable<MovieRes> movieList(String categoryId, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("contentSubType", "Movie");
        if(null != categoryId) {
            queryMap.put("category", categoryId);
        }
        Observable<MovieRes> observable = dataManager.getProductService().movieList(queryMap);
        return observable;
    }

    public Observable<MovieRes> tvShowList(String categoryId, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("contentSubType", "Series");
        if(null != categoryId) {
            queryMap.put("category", categoryId);
        }
        Observable<MovieRes> observable = dataManager.getProductService().tvShowList(queryMap);
        return observable;
    }

    public Observable<CollectionRes> collectionList(String userId, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("limit", "10");
        queryMap.put("user", userId);
        Observable<CollectionRes> observable = dataManager.getProductService().collectionList(queryMap);
        return observable;
    }


  /*  public Observable<MovieRes> tvLiveTvList(String userId, String categoryId, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("contentSubType", "TV");
        queryMap.put("user", userId);
        if(null != categoryId) {
            queryMap.put("category", categoryId);
        }
        Observable<MovieRes> observable = dataManager.getProductService().tvLiveTvList(queryMap);
        return observable;
    }

    public Observable<MovieRes> collectionFavList(String userId, String contentSubType, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("contentSubType", contentSubType);
        queryMap.put("user", userId);
        Observable<MovieRes> observable = dataManager.getProductService().collectionFavList(queryMap);
        return observable;
    }

    public Observable<MovieRes> searchList(String userId , String contentSubType , String search, DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        queryMap.put("contentSubType", contentSubType);
        queryMap.put("search", search);
        queryMap.put("user", userId);
        Observable<MovieRes> observable = dataManager.getProductService().searchList(queryMap);
        return observable;
    }

    public Observable<ResAuthoriz> isAuthorized(ReqAuthoriz reqAuthoriz, DataManager dataManager) {
        Observable<ResAuthoriz> observable = dataManager.getProductService().isAuthorized(reqAuthoriz);
        return observable;
    }


    public Observable<CategoryRes> categoryList(Map<String, String> queryMap , DataManager dataManager) {
        setMIdPageParameter(queryMap);
        Observable<CategoryRes> observable = dataManager.getProductService().categoryList(queryMap);
        return observable;
    }

    public Observable<List<CategoryFrag>> categoryByTypeList(Map<String, String> queryMap , DataManager dataManager) {
        setMIdPageParameter(queryMap);
        Observable<List<CategoryFrag>> observable = dataManager.getProductService().categoryByTypeList(queryMap);
        return observable;
    }


    public Observable<SendOtpRes> sendOtp(SendOtpReq sendOtpReq, DataManager dataManager) {
        Observable<SendOtpRes> observable = dataManager.getProductService().sendOtp(sendOtpReq);
        return observable;
    }

    public Observable<VerifyOtpRes> verifyOtp(VerifyOtpReq verifyOtpReq, DataManager dataManager) {
        Observable<VerifyOtpRes> observable = dataManager.getProductService().verifyOtp(verifyOtpReq);
        return observable;
    }

    public Observable<DetailRes> movieDetail(String userId, String movieId , DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("user", userId);
        setMIdPageParameter(queryMap);
        Observable<DetailRes> observable = dataManager.getProductService().movieDetail(movieId,queryMap);
        return observable;
    }

    //http://gateway.amtv.id/amtvservices/amtv-services/api/v1/userlikes
    public Observable<WatchRes> watchVideo(WatchReq watchReq, DataManager dataManager) {
        Observable<WatchRes> observable = dataManager.getProductService().watchVideo(watchReq);
        return observable;
    }


    public Observable<MySubscribeRes> mySubscribe(String userId, DataManager dataManager) {
        Observable<MySubscribeRes> observable = dataManager.getProductService().mySubscribe(userId);
        return observable;
    }


    public Observable<List<City>> cityList(String countryId , DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("page","0");
        queryMap.put("size","100");
        Observable<List<City>> observable = dataManager.getProductService().cityList(countryId,queryMap);
        return observable;
    }


    public Observable<SubscribeDetailRes> callSubscribeDetail(String userId , DataManager dataManager) {
        Map<String, String> queryMap = new HashMap<>();
        setMIdPageParameter(queryMap);
        Observable<SubscribeDetailRes> observable = dataManager.getProductService().callSubscribeDetail(userId);
        return observable;
    }*/

    private void setMIdPageParameter(Map<String, String> queryMap) {
        queryMap.put("mid", Constants.MARKET_PLACE_ID);
        queryMap.put("page", String.valueOf(Constants.NODE_FIRST_PAGE_INDEX));
        queryMap.put("limit", String.valueOf(Constants.NODE_MAX_PAGE_LIMIT));
    }

}
