package amtv.iwant.amtvtv.data.network.model.subscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscibeReq {

    @SerializedName("packages")
    @Expose

    private List<Package> packages = null;
    @SerializedName("mid")
    @Expose
    private String mid;

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }
}
