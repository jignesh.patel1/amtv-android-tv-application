package amtv.iwant.amtvtv.data.network.model.featured;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

    @SerializedName("contentURL")
    @Expose
    private String contentURL;
    @SerializedName("duration")
    @Expose
    private Object duration;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("aspectRatio")
    @Expose
    private String aspectRatio;
    @SerializedName("streamingType")
    @Expose
    private String streamingType;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public String getContentURL() {
        return contentURL;
    }

    public void setContentURL(String contentURL) {
        this.contentURL = contentURL;
    }

    public Object getDuration() {
        return duration;
    }

    public void setDuration(Object duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(String aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public String getStreamingType() {
        return streamingType;
    }

    public void setStreamingType(String streamingType) {
        this.streamingType = streamingType;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
