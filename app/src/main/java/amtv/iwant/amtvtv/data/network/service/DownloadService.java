package amtv.iwant.amtvtv.data.network.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;
import amtv.iwant.amtvtv.data.network.model.download.Download;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;

public class DownloadService extends IntentService {

    private Context context;

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;

    private DataManager dataManager = null;
    private CompositeDisposable compositeDisposable = null;

    @Override
    public void onCreate() {
        super.onCreate();
        context = DownloadService.this;
        dataManager = new AppDataManager(context);
        compositeDisposable = new CompositeDisposable();
    }

    // https://stackoverflow.com/questions/41892696/is-it-possible-to-show-progress-bar-when-download-via-retrofit-2-asynchronous

    @Override
    protected void onHandleIntent(Intent intent) {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentTitle("Download")
                .setContentText("Downloading File")
                .setOngoing(true)
                .setProgress(100, 1, true)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        /*if(intent.getExtras() != null && intent.getExtras().containsKey(ProductDetailActivity.PRODUCT_BROCHURE)) {
            String url = intent.getExtras().getString(ProductDetailActivity.PRODUCT_BROCHURE);
            String productId = intent.getExtras().getString(ProductDetailActivity.KEY_PRODUCT_ID);
            initDownload(productId, url);
        }*/
    }

   /* private void initDownload(final String productId, final String url){

        Call<ResponseBody> call = dataManager.getProductService().downloadFile(url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        downloadFile(productId, url, response.body());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                RestErrorConsumer.getRestErrorFromThrowable(t);
            }
        });
    }*/

    private void downloadFile(String productId, String url, ResponseBody body) throws IOException {

        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), getFileName(productId, url));
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
                sendNotification(download);
                timeCount++;
            }

            output.write(data, 0, count);
        }
        onDownloadComplete(outputFile);
        output.flush();
        output.close();
        bis.close();

    }

    private String getFileName(String productId, String url) {
        String fileExtension = "";
        int pos = url.lastIndexOf(".");

        if(pos != -1) {
             fileExtension = url.substring(pos);
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Brochure_");
        stringBuilder.append(productId);
        stringBuilder.append(fileExtension);

        return stringBuilder.toString();
    }

    private void sendNotification(Download download){

        //sendIntent(download);
        notificationBuilder.setOngoing(true);
        notificationBuilder.setProgress(100,download.getProgress(),false);
        notificationBuilder.setContentText("Downloading file "+ download.getCurrentFileSize() +"/"+totalFileSize +" MB");
        notificationManager.notify(0, notificationBuilder.build());
    }

    /*private void sendIntent(Download download){

        Intent intent = new Intent(ProductDetailActivity.MESSAGE_PROGRESS);
        intent.putExtra("download",download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }*/

    private void onDownloadComplete(File outputFile){

        Download download = new Download();
        download.setProgress(100);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mimeType = "";
        if(null != outputFile && null != outputFile.toString()){
            String extension = MimeTypeMap.getFileExtensionFromUrl(outputFile.toString());
            if(extension != null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        }
        if(TextUtils.isEmpty(mimeType)) {
            mimeType = "application/pdf";
        }

        intent.setDataAndType(Uri.fromFile(outputFile), mimeType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationManager.cancel(0);
        notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done);
        notificationBuilder.setOngoing(false);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setProgress(0,0,false);
        notificationBuilder.setContentText("File Downloaded");
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

}