package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amtv.iwant.amtvtv.data.network.model.base.BaseRes;

/**
 * Created by manish on 16/3/18.
 */

public class EmailVerificationRes extends BaseRes {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("verified")
    @Expose
    public boolean verified;
    @SerializedName("email")
    @Expose
    public String email;

    public String getId() {
        return id;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getEmail() {
        return email;
    }
}
