package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDto implements Serializable {


    @SerializedName("firstName")
    @Expose
    public String firstName;
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("mobileCountryCode")
    @Expose
    public String mobileCountryCode;
    @SerializedName("mobileNo")
    @Expose
    public String mobileNo;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;
    @SerializedName("iconImage")
    @Expose
    public String iconImage;
    @SerializedName("lastName")
    @Expose
    public String lastName;
    @SerializedName("userProfile")
    @Expose
    public UserProfile userProfile;

    @SerializedName("gender")
    @Expose
    public String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIconImage() {
        return iconImage;
    }

    public void setIconImage(String iconImage) {
        this.iconImage = iconImage;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}