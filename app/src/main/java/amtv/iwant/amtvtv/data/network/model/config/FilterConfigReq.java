package amtv.iwant.amtvtv.data.network.model.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amtv.iwant.amtvtv.data.network.model.base.BaseReq;

/**
 * Created by manish on 9/11/17.
 */

public class FilterConfigReq extends BaseReq {

    @SerializedName("appName")
    @Expose
    private AppName appName;
    @SerializedName("businessConfigurationId")
    @Expose
    private BusinessConfigurationId businessConfigurationId;

    public AppName getAppName() {
        return appName;
    }

    public void setAppName(AppName appName) {
        this.appName = appName;
    }

    public BusinessConfigurationId getBusinessConfigurationId() {
        return businessConfigurationId;
    }

    public void setBusinessConfigurationId(BusinessConfigurationId businessConfigurationId) {
        this.businessConfigurationId = businessConfigurationId;
    }


}
