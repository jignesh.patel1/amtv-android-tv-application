package amtv.iwant.amtvtv.data.network.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Address implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("addressType")
    @Expose
    public String addressType;
    @SerializedName("addressTitle")
    @Expose
    public String addressTitle;
    @SerializedName("addressLine")
    @Expose
    public String addressLine;
    @SerializedName("flatNameNo")
    @Expose
    public String flatNameNo;
    @SerializedName("streetName")
    @Expose
    public String streetName;
    @SerializedName("landMark")
    @Expose
    public String landMark;
    @SerializedName("pincode")
    @Expose
    public Integer pincode;
    @SerializedName("isDefault")
    @Expose
    public Boolean isDefault;
    @SerializedName("cityId")
    @Expose
    public Integer cityId;
    @SerializedName("cityName")
    @Expose
    public String cityName;
    @SerializedName("stateId")
    @Expose
    public Integer stateId;
    @SerializedName("stateName")
    @Expose
    public String stateName;
    @SerializedName("countryId")
    @Expose
    public Integer countryId;
    @SerializedName("countryName")
    @Expose
    public String countryName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getFlatNameNo() {
        return flatNameNo;
    }

    public void setFlatNameNo(String flatNameNo) {
        this.flatNameNo = flatNameNo;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}