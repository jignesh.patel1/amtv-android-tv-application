package amtv.iwant.amtvtv.data.network.model.subscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscribeDetailRes {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("subscriptionId")
    @Expose
    private Integer subscriptionId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("discountAmount")
    @Expose
    private Integer discountAmount;
    @SerializedName("dailySubscriptionCharge")
    @Expose
    private Integer dailySubscriptionCharge;
    @SerializedName("usedDays")
    @Expose
    private Integer usedDays;
    @SerializedName("remainingDays")
    @Expose
    private Integer remainingDays;
    @SerializedName("gracePeriodDays")
    @Expose
    private Integer gracePeriodDays;
    @SerializedName("isOnGracePeriod")
    @Expose
    private Boolean isOnGracePeriod;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("startAt")
    @Expose
    private String startAt;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getDailySubscriptionCharge() {
        return dailySubscriptionCharge;
    }

    public void setDailySubscriptionCharge(Integer dailySubscriptionCharge) {
        this.dailySubscriptionCharge = dailySubscriptionCharge;
    }

    public Integer getUsedDays() {
        return usedDays;
    }

    public void setUsedDays(Integer usedDays) {
        this.usedDays = usedDays;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public Integer getGracePeriodDays() {
        return gracePeriodDays;
    }

    public void setGracePeriodDays(Integer gracePeriodDays) {
        this.gracePeriodDays = gracePeriodDays;
    }

    public Boolean getIsOnGracePeriod() {
        return isOnGracePeriod;
    }

    public void setIsOnGracePeriod(Boolean isOnGracePeriod) {
        this.isOnGracePeriod = isOnGracePeriod;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
