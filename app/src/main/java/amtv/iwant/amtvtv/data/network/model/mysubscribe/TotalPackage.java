package amtv.iwant.amtvtv.data.network.model.mysubscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TotalPackage {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("packageId")
    @Expose
    private Integer packageId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("packageType")
    @Expose
    private String packageType;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("isDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("expiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("validityInDays")
    @Expose
    private Integer validityInDays;
    @SerializedName("standardValidityInDays")
    @Expose
    private Integer standardValidityInDays;
    @SerializedName("trial")
    @Expose
    private Object trial;
    @SerializedName("parent")
    @Expose
    private List<Object> parent = null;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("subscriptionType")
    @Expose
    private String subscriptionType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getValidityInDays() {
        return validityInDays;
    }

    public void setValidityInDays(Integer validityInDays) {
        this.validityInDays = validityInDays;
    }

    public Integer getStandardValidityInDays() {
        return standardValidityInDays;
    }

    public void setStandardValidityInDays(Integer standardValidityInDays) {
        this.standardValidityInDays = standardValidityInDays;
    }

    public Object getTrial() {
        return trial;
    }

    public void setTrial(Object trial) {
        this.trial = trial;
    }

    public List<Object> getParent() {
        return parent;
    }

    public void setParent(List<Object> parent) {
        this.parent = parent;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

}