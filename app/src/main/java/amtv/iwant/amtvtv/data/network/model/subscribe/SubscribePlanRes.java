package amtv.iwant.amtvtv.data.network.model.subscribe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscribePlanRes {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("packageId")
    @Expose
    private Integer packageId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("packageType")
    @Expose
    private String packageType;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("isDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("validityInDays")
    @Expose
    private Integer validityInDays;
    @SerializedName("coverImageURLs")
    @Expose
    private List<Object> coverImageURLs = null;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("parent")
    @Expose
    private List<Object> parent = null;
    @SerializedName("subscriptionType")
    @Expose
    private String subscriptionType;
    @SerializedName("addonPackages")
    @Expose
    private List<Object> addonPackages = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getValidityInDays() {
        return validityInDays;
    }

    public void setValidityInDays(Integer validityInDays) {
        this.validityInDays = validityInDays;
    }

    public List<Object> getCoverImageURLs() {
        return coverImageURLs;
    }

    public void setCoverImageURLs(List<Object> coverImageURLs) {
        this.coverImageURLs = coverImageURLs;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Object> getParent() {
        return parent;
    }

    public void setParent(List<Object> parent) {
        this.parent = parent;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public List<Object> getAddonPackages() {
        return addonPackages;
    }

    public void setAddonPackages(List<Object> addonPackages) {
        this.addonPackages = addonPackages;
    }

}
