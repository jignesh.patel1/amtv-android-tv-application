package amtv.iwant.amtvtv.data.network.model.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amtv.iwant.amtvtv.data.network.model.movie.Actor;
import amtv.iwant.amtvtv.data.network.model.movie.Category;
import amtv.iwant.amtvtv.data.network.model.movie.Director;
import amtv.iwant.amtvtv.data.network.model.movie.Genre;
import amtv.iwant.amtvtv.data.network.model.movie.Metadata;
import amtv.iwant.amtvtv.data.network.model.movie.RatingsMetadata;

public class DetailRes {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("imageURL")
    @Expose
    private String imageURL;
    @SerializedName("publishingStart")
    @Expose
    private String publishingStart;
    @SerializedName("publishingEnd")
    @Expose
    private String publishingEnd;
    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("trailerURL")
    @Expose
    private String trailerURL;
    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;
    @SerializedName("productionZone")
    @Expose
    private String productionZone;
    @SerializedName("producedOn")
    @Expose
    private String producedOn;
    @SerializedName("imdbId")
    @Expose
    private String imdbId;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("contentId")
    @Expose
    private String contentId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("contentSubType")
    @Expose
    private String contentSubType;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("genres")
    @Expose
    private List<Genre> genres = null;
    @SerializedName("ratingsMetadata")
    @Expose
    private RatingsMetadata ratingsMetadata;
    @SerializedName("isFeatured")
    @Expose
    private Boolean isFeatured;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("coverImageURLs")
    @Expose
    private List<String> coverImageURLs = null;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("directors")
    @Expose
    private List<Director> directors = null;
    @SerializedName("actors")
    @Expose
    private List<Actor> actors = null;
    @SerializedName("isLike")
    @Expose
    private Boolean isLike;
    @SerializedName("isPlayable")
    @Expose
    private Boolean isPlayable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getPublishingStart() {
        return publishingStart;
    }

    public void setPublishingStart(String publishingStart) {
        this.publishingStart = publishingStart;
    }

    public String getPublishingEnd() {
        return publishingEnd;
    }

    public void setPublishingEnd(String publishingEnd) {
        this.publishingEnd = publishingEnd;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getProductionZone() {
        return productionZone;
    }

    public void setProductionZone(String productionZone) {
        this.productionZone = productionZone;
    }

    public String getProducedOn() {
        return producedOn;
    }

    public void setProducedOn(String producedOn) {
        this.producedOn = producedOn;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getContentSubType() {
        return contentSubType;
    }

    public void setContentSubType(String contentSubType) {
        this.contentSubType = contentSubType;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public RatingsMetadata getRatingsMetadata() {
        return ratingsMetadata;
    }

    public void setRatingsMetadata(RatingsMetadata ratingsMetadata) {
        this.ratingsMetadata = ratingsMetadata;
    }

    public Boolean getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public List<String> getCoverImageURLs() {
        return coverImageURLs;
    }

    public void setCoverImageURLs(List<String> coverImageURLs) {
        this.coverImageURLs = coverImageURLs;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public Boolean getIsPlayable() {
        return isPlayable;
    }

    public void setIsPlayable(Boolean isPlayable) {
        this.isPlayable = isPlayable;
    }

}
