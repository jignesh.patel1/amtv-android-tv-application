package amtv.iwant.amtvtv.data.network;

import com.google.gson.Gson;

import amtv.iwant.amtvtv.data.network.service.ProductService;
import amtv.iwant.amtvtv.data.network.service.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebService
{
    private static WebService instance;
    private UserService userService;
    private ProductService productService;
    private String baseUrl = new String();

    public static WebService getInstance()
    {
        if (instance == null)
        {
            instance = new WebService();
        }
        return instance;
    }

    private WebService()
    {
       // baseUrl = AppSettings.getInstance().getBaseUrl();
       // init(baseUrl);
    }

    public void initUser(String baseUrl)
    {
        this.baseUrl = baseUrl;
        Gson gson = GsonInterface.getInstance().getGson();
        Retrofit serviceRetrofit = createUserRetrofit(getBaseUrl(), gson);
        userService = serviceRetrofit.create(UserService.class);
    }
    public void initProduct(String baseUrl)
    {
        this.baseUrl = baseUrl;
        Gson gson = GsonInterface.getInstance().getGson();
        Retrofit serviceRetrofit = createUserRetrofit(getBaseUrl(), gson);
        productService = serviceRetrofit.create(ProductService.class);
    }

    /*public void init(List<String> baseUrl, OkHttpClient okHttpClient)
    {
        this.baseUrl = baseUrl;
        Gson gson = GsonInterface.getInstance().getGson();
        Retrofit serviceRetrofit = createRetrofit(getBaseUrl(), gson, okHttpClient);
        userService = serviceRetrofit.create(IService.class);
    }*/

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl)
    {
        this.baseUrl = baseUrl;
    }


    /*protected Retrofit createRetrofit(String baseUrl, Gson gson, OkHttpClient okHttpClient)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit;
    }*/

    protected Retrofit createUserRetrofit(String baseUrl, Gson gson)
    {
        OkHttpClient okHttpClient = HttpClientController.getInstance().getHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }

    public UserService getUserService()
    {
        return userService;
    }

    public ProductService getProductService()
    {
        return productService;
    }

}
