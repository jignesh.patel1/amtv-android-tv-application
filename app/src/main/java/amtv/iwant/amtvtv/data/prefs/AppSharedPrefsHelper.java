package amtv.iwant.amtvtv.data.prefs;


import android.content.SharedPreferences;

import amtv.iwant.amtvtv.core.Constants;

public class AppSharedPrefsHelper implements SharedPrefsHelper {

    public static String PREF_KEY_ACCESS_TOKEN = "access-token";

    private SharedPreferences mSharedPreferences;

    public AppSharedPrefsHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public void put(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public void put(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    public void put(String key, float value) {
        mSharedPreferences.edit().putFloat(key, value).apply();
    }

    public void put(String key, boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public String get(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public Integer get(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }

    public Float get(String key, float defaultValue) {
        return mSharedPreferences.getFloat(key, defaultValue);
    }

    public Boolean get(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public void deleteSavedData(String key) {
        mSharedPreferences.edit().remove(key).apply();
    }

    public void deleteLoginData() {
        mSharedPreferences.edit().remove(Constants.PREF_KEY_IS_LOGGED_IN).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_OAUTH_TOKEN).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_OAUTH_TOKEN_TYPE).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_ID).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_COUNTRY_CODE).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_EMAIL).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_NAME).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_MOBILE).apply();
        mSharedPreferences.edit().remove(Constants.PREF_KEY_USER_IMG).apply();
    }

    public void deleteLocationData() {

    }

    public void clearAll() {
        mSharedPreferences.edit().clear().commit();
    }

}