package amtv.iwant.amtvtv.data.network.event;


import amtv.iwant.amtvtv.data.network.model.base.ApiError;

public class RestError
{
    private RestErrorType restErrorType = RestErrorType.NONE;
    private ApiError error;

    public RestErrorType getRestErrorType()
    {
        return restErrorType;
    }

    public void setRestErrorType(RestErrorType restErrorType)
    {
        this.restErrorType = restErrorType;
    }

    public ApiError getError()
    {
        return error;
    }

    public void setError(ApiError error)
    {
        this.error = error;
    }
}
