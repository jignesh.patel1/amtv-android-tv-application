package amtv.iwant.amtvtv.data.network.model.collection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Content implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("imageURL")
    @Expose
    private String imageURL;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contentUrl")
    @Expose
    private String contentUrl;
    @SerializedName("streamingType")
    @Expose
    private String streamingType;
    @SerializedName("watchCount")
    @Expose
    private Integer watchCount;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isLike")
    @Expose
    private boolean isLike;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getStreamingType() {
        return streamingType;
    }

    public void setStreamingType(String streamingType) {
        this.streamingType = streamingType;
    }

    public Integer getWatchCount() {
        return watchCount;
    }

    public void setWatchCount(Integer watchCount) {
        this.watchCount = watchCount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
