package amtv.iwant.amtvtv.data.network;

import com.google.gson.stream.MalformedJsonException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import amtv.iwant.amtvtv.core.AmtvApp;
import amtv.iwant.amtvtv.data.network.event.RestError;
import amtv.iwant.amtvtv.data.network.event.RestErrorType;
import amtv.iwant.amtvtv.data.network.model.base.ApiError;
import amtv.iwant.amtvtv.data.network.networkexception.InternetNotAvailableException;
import amtv.iwant.amtvtv.data.network.networkexception.NetworkNotAvailableException;
import amtv.iwant.amtvtv.ui.custom.MyToast;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by manish on 8/11/17.
 */

public abstract class RestErrorConsumer implements Consumer<Throwable> {

    @Override
    public void accept(@NonNull Throwable throwable) throws Exception {

        RestError restError = getRestErrorFromThrowable(throwable);
        error(restError);
    }

    public abstract void error(RestError restError);

    public static RestError getRestErrorFromThrowable(Throwable throwable)
    {
        RestError restError = new RestError();
        ApiError error = new ApiError();
        if (throwable instanceof NetworkNotAvailableException)
        {
            restError.setRestErrorType(RestErrorType.NETWORK_NOT_AVAILABLE);
            error.setMessage("Network not available. Please try again.");
            error.setCode("410");
        } else if (throwable instanceof InternetNotAvailableException)
        {
            restError.setRestErrorType(RestErrorType.INTERNET_NOT_AVAILABLE);
            error.setMessage("Internet is not connected. Please try again.");
            error.setCode("430");
        } else if (throwable instanceof java.net.ConnectException)
        {
            // restError.setRestErrorType(RestErrorType.INTERNET_NOT_AVAILABLE);

            restError.setRestErrorType(RestErrorType.REST_API_ERROR);
            error.setMessage("we cannot connect to server.Please contact admin.");
            error.setCode("400");

        } else if (throwable instanceof SocketTimeoutException)
        {
            restError.setRestErrorType(RestErrorType.REST_API_ERROR);
            error.setMessage("we cannot connect to server.Please contact admin.");
            error.setCode("400");

        } else if (throwable instanceof UnknownHostException)
        {
            restError.setRestErrorType(RestErrorType.NETWORK_NOT_AVAILABLE);
        } else if (throwable instanceof MalformedJsonException)
        {
            restError.setRestErrorType(RestErrorType.REST_API_ERROR);
        } else if (throwable instanceof IOException)
        {
            restError.setRestErrorType(RestErrorType.NETWORK_NOT_AVAILABLE);
        } else if (throwable instanceof HttpException)
        {
            restError.setRestErrorType(RestErrorType.REST_API_ERROR);
            HttpException httpException = (HttpException) throwable;
            try
            {
                if(((HttpException) throwable).code() == 401 && ((HttpException) throwable).response().errorBody().string().contains("invalid_token"))
                {
                    error.setMessage("Error, Some Issues.. Kindly Logout and Login again!!");
                    error.setCode("401");
                }
                else
                {
                    error = GsonInterface.getInstance().getGson().fromJson(((HttpException) throwable).response().errorBody().string(), ApiError.class);
                }
            } catch (Exception ex)
            {
                ex.printStackTrace();
            }

        } else
        {
            restError.setRestErrorType(RestErrorType.NETWORK_NOT_AVAILABLE);
        }
        throwable.printStackTrace();
        Timber.d("API Error:" + throwable.toString());

        if(error != null && error.getDetail() != null && error.getDetail().toString().trim().length() > 0)
        {
            restError.setError(error);
            error.setCode(error.getStatus());
            error.setMessage(error.getDetail());
        }
        else if(error != null && error.getMessage() != null && error.getMessage().toString().trim().length() > 0)
        {
            restError.setError(error);
        } else {
            error = new ApiError();
            error.setCode("420");
            error.setMessage("OOPS! Something went wrong.");
            restError.setError(error);
        }

        if(restError.getError().getCode().equals("IW-BUSRUSR028")) {

        } else {
            try {
                MyToast.showToast(AmtvApp.getContext(), restError.getError().getMessage());
            } catch (Exception e) {

            }

        }

        return restError;
    }
}
