/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package amtv.iwant.amtvtv;

import butterknife.BindView;
import butterknife.ButterKnife;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v17.leanback.app.VideoSupportFragment;
import android.support.v17.leanback.app.VideoSupportFragmentGlueHost;
import android.support.v17.leanback.media.MediaPlayerAdapter;
import android.support.v17.leanback.media.PlaybackTransportControlGlue;
import android.support.v17.leanback.widget.PlaybackControlsRow;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;

import amtv.iwant.amtvtv.data.network.model.collection.Content;

/**
 * Handles video playback with media controls.
 */
public class PlaybackVideoFragment extends Fragment implements PlaybackPreparer,
        ExoPlayer.EventListener, PlayerControlView.VisibilityListener,
        DialogInterface.OnClickListener  {

  //  private PlaybackTransportControlGlue<MediaPlayerAdapter> mTransportControlGlue;

    // Exo Player

    private Context context = null;
    public static final String TAG = "TV_PLAYER";

    @BindView(R.id.player_view)
    PlayerView playerView;
    private DefaultTrackSelector trackSelector;
    private DefaultRenderersFactory renderersFactory;
    private MediaSource videoSource;
    private SimpleExoPlayer exoPlayer;

    private String vURL = "";
    private final String proxyURL = "https://cors-anywhere.herokuapp.com/";
    private boolean useProxy = false;
    private DefaultLoadControl loadControl;
    private DebugTextViewHelper debugViewHelper;

    private DataSource.Factory mediaDataSourceFactory;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private String userAgent;

    // Cache helper Properties
    private File downloadDirectory;
    private Cache downloadCache;

    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private static final String DOWNLOAD_CONTENT_DIRECTORY = "/AMTV1";

    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    //  private static final String KEY_AUTO_PLAY = "auto_play

    private TextView textBandwidth;
    private TextView textStatus;
    private TextView textTrackInfo;

    private int startWindow;
    private long startPosition;
    private boolean startAutoPlay;

    private boolean backToScreen = false;
    private boolean isTabActive = true;
    private boolean isPlayStarted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  VideoSupportFragmentGlueHost glueHost =
                new VideoSupportFragmentGlueHost(PlaybackVideoFragment.this);

        MediaPlayerAdapter playerAdapter = new MediaPlayerAdapter(getActivity());
        playerAdapter.setRepeatAction(PlaybackControlsRow.RepeatAction.INDEX_NONE);

        mTransportControlGlue = new PlaybackTransportControlGlue<>(getActivity(), playerAdapter);
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(movie.getTitle());
        mTransportControlGlue.setSubtitle(Html.fromHtml(movie.getDescription()));
        mTransportControlGlue.playWhenPrepared();
        playerAdapter.setDataSource(Uri.parse(movie.getContentUrl()));*/

     //   setUp(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_video_player,container, false);
        ButterKnife.bind(this, view);

        setUp(savedInstanceState);

        return view;
    }


    private void setUp(Bundle savedInstanceState) {

        final Content content =
                (Content) getActivity().getIntent().getSerializableExtra(DetailsActivity.MOVIE);

        context = getActivity();

        vURL = content.getContentUrl();

        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = true; //savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            Log.d(TAG, "Resume from Saved Instance:" + startPosition + "," + startWindow);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            Log.d(TAG, "Starting New Track");
            clearStartPosition("On Create();");
        }
        /*
        Separate thread not needed. hence commeting.
        SmartPlayer playerThread = new SmartPlayer();
        playerThread.start();
        */
        createPlayer();

    }

    private void createPlayer() {
        // Binding the Player View Object from Resource
        playerView.setShowBuffering(false);
        playerView.setResizeMode(3);
        playerView.requestFocus();
    }

    private void initPlayback(String event) {
        if (!isTabActive) {
            Log.d(TAG, "TAB IS NOT ACTIVE, SO NO NEED TO START PLAY BACK");
            return;
        }

        isPlayStarted = true;
        Log.d(TAG, "START PLAYBACK: " + event);
        //  useProxy = switchProxy.isChecked();
        useProxy = false;
        renderersFactory = null;
        renderersFactory = new DefaultRenderersFactory(context,
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER);

        // Step 4: Crate Default Load Control
        loadControl = null;
        loadControl = new DefaultLoadControl.Builder().createDefaultLoadControl();

        // Step 5: Create Track selector

        trackSelector = null;
        trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(BANDWIDTH_METER));


        Log.d(TAG, "Forcing Lowest bitrate");
        DefaultTrackSelector.Parameters newParam = trackSelector.getParameters().buildUpon().setForceLowestBitrate(true).build();
        trackSelector.setParameters(newParam);


        vURL = vURL.replace("http://ott.amdc.tech:8080/", "");

        String videoSourceURI = (useProxy) ? proxyURL + vURL : vURL;
        Log.d(TAG, "URI:" + videoSourceURI);
        Log.d(TAG, "Creating Cache Data Source");

        resetDataSource();

        Log.d(TAG, "Creating HLS Media Source Source");

        if (null != videoSourceURI && !videoSourceURI.isEmpty()) {
            videoSource = new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(Uri.parse(videoSourceURI));

            /*videoSource = new ExtractorMediaSource(Uri.parse(videoSourceURI),
                    new CacheDataSourceFactory(context, 100 * 1024 * 1024, 5 * 1024 * 1024),
                    new DefaultExtractorsFactory(), null, null);*/

        }

        if (null != videoSource) {

            Log.d(TAG, "Media Source Created:" + videoSource.toString());
            //Step 6: // Create Exoplayer
            Log.d(TAG, "Creating Player:");
            exoPlayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
            Log.d(TAG, "Player Created Successfully with Default Tract Selector");
            // Step 7: Attach Media Source with Exo Player
            exoPlayer.prepare(videoSource);
            playerView.setPlayer(exoPlayer);

            playerView.setControllerAutoShow(true);
            playerView.setControllerShowTimeoutMs(1000);

            //  currentView.setText("Ready to play " + currentName);
            //  Log.d(TAG, "Ready to play " + currentName);
            //  currentView.setText(currentName);
            // Step 8: Attach the Event Listener

            // Step 9: Set the player for Autoplay as soon as buffer is available to play
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.addListener(this);

            //debugViewHelper = new DebugTextViewHelper(exoPlayer, BANDWIDTH_METER, textStatus, textTrackInfo, textBandwidth);
            //debugViewHelper = new DebugTextViewHelper(exoPlayer, textStatus);
            //debugViewHelper.start();
        } else {
            Log.d(TAG, "Media Source is Null");
        }
    }

    private void resetDataSource() {
        HttpDataSource.Factory upstreamFactory = new DefaultHttpDataSourceFactory(userAgent, BANDWIDTH_METER);
        if (useProxy) {
            Log.d(TAG, "Http Proxy Streaming Enabled");
            upstreamFactory.getDefaultRequestProperties().set("X-Requested-With", "HttpRequest");
        }
        //Step 2.2. Create the Cache Source Factory with Finding the HTTP Data Source Factory

        CacheDataSourceFactory cacheFactory = new CacheDataSourceFactory(getDownloadCache(),
                upstreamFactory,
                new FileDataSourceFactory(),
                null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                null);

        mediaDataSourceFactory = cacheFactory;
    }

    /*
   This section is for the cache manager only
    */
    private synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            Log.d(TAG, "downloadCache=null:" + downloadContentDirectory.getAbsolutePath());
            if (null != downloadContentDirectory)
                try {
                    downloadCache = new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor());
                } catch (java.lang.IllegalStateException e) {
                    File newdownloadContentDirectory = new File(getDownloadDirectory(), "ON_ERROR_HOME" + Math.random());
                    downloadCache = new SimpleCache(newdownloadContentDirectory, new NoOpCacheEvictor());
                    Log.d(TAG, "IGNORING CACHE ERROR");
                }
        } else {
            Log.d(TAG, "Cache is Available");
        }
        return downloadCache;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = context.getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = context.getFilesDir();
            }
        }
        return downloadDirectory;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                !playWhenReady) {

            playerView.setKeepScreenOn(false);
        } else { // STATE_IDLE, STATE_ENDED
            // This prevents the screen from getting dim/lock
            playerView.setKeepScreenOn(true);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        //  Toast.makeText(getApplicationContext(),"Error! Please check internet connection.",Toast.LENGTH_LONG).show();
        if (exoPlayer != null) {
            updateStartPosition();
            //releasePlayer(":OnPlayerError():");
            if (videoSource != null && exoPlayer != null) {
                exoPlayer.prepare(videoSource, false, false);
            } else {
                initPlayback("ON ERROR");
            }
        }

    }

    @Override
    public void onPositionDiscontinuity(int reason) {
        updateStartPosition();
    }

    @Override
    public void onSeekProcessed() {

    }


    @Override
    public void preparePlayback() {

    }

    @Override
    public void onVisibilityChange(int visibility) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "OnStop Event");
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            releasePlayer("Activity: OnStop()");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        /*if (mTransportControlGlue != null) {
            mTransportControlGlue.pause();
        }*/

        Log.d(TAG, "onPause() Event");
        Log.d(TAG, "Reset Exoplayer : onPause(): SDK_INT:(" + Util.SDK_INT + "):" + exoPlayer);
        releasePlayer("Activity: onPause");
        logPosition();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (exoPlayer == null) {
            initPlayback("RESUME_PLAY");
        }
    }

    private void releasePlayer(String event) {
        Log.d(TAG, "RELEASE-PLAYER:" + event);
        if (exoPlayer != null) {
            Log.d(TAG, "- Releasing Exo Player:" + event);
            updateTrackSelectorParameters();
            updateStartPosition();
            videoSource = null;
            exoPlayer.release();
            exoPlayer = null;
            debugViewHelper.stop();
            debugViewHelper = null;
            isPlayStarted = false;
            logPosition();
        } else {
            Log.d(TAG, "RELEASE-PLAYER:EXO-PLAYER IS NULL");
        }
    }

    private void updateTrackSelectorParameters() {
        Log.d(TAG, "updateTrackSelectorParameters()");
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }

    private void clearStartPosition(String event) {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
        Log.d(TAG, "Clear Start Position: Event" + event);
        logPosition();
    }

    /*
   Additional Functions for smooth plaback
    */
    private void updateStartPosition() {
        if (exoPlayer != null) {
            startAutoPlay = exoPlayer.getPlayWhenReady();
            startWindow = exoPlayer.getCurrentWindowIndex();
            startPosition = Math.max(0, exoPlayer.getContentPosition());
            Log.d(TAG, "StartPosition Updated:" + startPosition + "," + startWindow);
        } else {
            Log.d(TAG, "updateStartPosition() without anything");
        }
    }

    private void logPosition() {
        Log.d(TAG, "Start Position:" + startPosition);
        Log.d(TAG, "Start Window:" + startWindow);
        Log.d(TAG, "Start Auto Play:" + startAutoPlay);
    }

    /*class CacheDataSourceFactory implements DataSource.Factory {
        private final Context context;
        private final DefaultDataSourceFactory defaultDatasourceFactory;
        private final long maxFileSize, maxCacheSize;

        CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                    bandwidthMeter,
                    new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
        }

        @Override
        public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
          //  SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media"), evictor);
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            try {
                downloadCache = new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor());
            } catch (java.lang.IllegalStateException e) {
                File newdownloadContentDirectory = new File(getDownloadDirectory(), "ON_ERROR_HOME" + Math.random());
                downloadCache = new SimpleCache(newdownloadContentDirectory, new NoOpCacheEvictor());
                Log.d(TAG, "IGNORING CACHE ERROR");
            }
            return new CacheDataSource(downloadCache, defaultDatasourceFactory.createDataSource(),
                    new FileDataSource(), new CacheDataSink(downloadCache, maxFileSize),
                    CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
        }
    }*/


}