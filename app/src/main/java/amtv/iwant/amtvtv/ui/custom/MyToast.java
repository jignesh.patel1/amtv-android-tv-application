package amtv.iwant.amtvtv.ui.custom;

import android.content.Context;
import android.graphics.Color;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

/**
 * Created by manish on 13/11/17.
 */

public class MyToast {


    public static void showToast(Context context, String msg) {
       // Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

        new StyleableToast
                .Builder(context)
                .text(msg)
                .textColor(Color.WHITE)
                .backgroundColor(Color.GRAY)
                .build().show();
    }

    public static void showToast(Context context, String msg, String colorCode, String backgroundColorCode) {

    }

}
