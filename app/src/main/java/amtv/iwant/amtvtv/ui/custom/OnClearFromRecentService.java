package amtv.iwant.amtvtv.ui.custom;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import amtv.iwant.amtvtv.core.Constants;
import amtv.iwant.amtvtv.data.AppDataManager;
import amtv.iwant.amtvtv.data.DataManager;

public class OnClearFromRecentService extends Service {

    DataManager dataManager;
    private String mobileNumber;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ClearFromRecentService", "Service Started");
      //  Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("ClearFromRecentService", "Service Destroyed");
     //   Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");

        dataManager = new AppDataManager(this);
     //   Toast.makeText(this, "Service End", Toast.LENGTH_SHORT).show();

        mobileNumber = dataManager.getSharedPreference().get(Constants.PREF_KEY_USER_MOBILE,"");
        if(mobileNumber.equalsIgnoreCase("9427553311")) {
            dataManager.getSharedPreference().deleteLoginData();
        }
        //Code here
        stopSelf();
    }
}