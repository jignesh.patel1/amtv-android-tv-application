package amtv.iwant.amtvtv.core;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;

import amtv.iwant.amtvtv.BuildConfig;
import amtv.iwant.amtvtv.data.prefs.Prefs;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by manish on 17/10/17.
 */

public class AmtvApp extends MultiDexApplication {


    private static AmtvApp appContext;

    public static AmtvApp getContext()
    {
        return appContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        appContext = AmtvApp.this;
        Realm.init(appContext);

        //Dexter.initialize(appContext);
        PackageInfo pInfo = null;
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            new Prefs.Builder()
                    .setContext(getApplicationContext())
                    .setMode(ContextWrapper.MODE_PRIVATE)
                    .setPrefsName(pInfo.packageName)
                    .setUseDefaultSharedPreference(true)
                    .build();
        } catch (Exception e) {

        }

        if (BuildConfig.DEBUG)
        {
            Stetho.initializeWithDefaults(this);
            Timber.plant(new Timber.DebugTree());
        } else
        {
//            Timber.plant(new CrashlyticsTree());
        }

        //configureFressoImageLoader();
        setBaseUrl();
    }

    /*private void configureFressoImageLoader() {
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);
    }*/

    private void setBaseUrl()
    {
        //WebService.getInstance().init(Constants.baseUrl);
    }

    private Activity mCurrentActivity = null;
    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }

}
