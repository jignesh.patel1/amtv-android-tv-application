package amtv.iwant.amtvtv.core;

/**
 * Created by manish on 6/2/18.
 */

public class FireBaseConstants {

    /*
        params
     */

    public static String MOBILE_NO = "mobile_no";
    public static String USER_ID = "user_id";
    public static String CUSTOMER_NAME = "customer_name";
    public static String BROCHURE_LINK = "brochure_link";
    public static final String ORDER_NO = "order_no";
    public static final String PAYMENT_MODE = "payment_mode";
    public static final String AMOUNT = "amount";
    public static final String DELIVERY_TYPE = "delivery_type";

    /*
       event name

     */

    public static String DOWNLOAD_BROCHURE = "download_brochure";
    public static String BUY_NOW = "buy_now";
    public static String RATE_PRODUCT = "rate_product";
    public static final String COLLECTION_VIEW_ALL = "collection_view_all";
    public static final String CANCEL_ORDER = "cancel_order";

}
