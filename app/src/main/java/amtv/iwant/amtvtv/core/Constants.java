package amtv.iwant.amtvtv.core;

/**
 * Created by manish on 9/11/17.
 */

public class Constants {

    public static final String PREF_KEY_BUSINESS_ID = "PREF_KEY_BUSINESS_ID";
    public static final String PREF_CONFIG_ID = "PREF_CONFIG_ID";

    public static final String PREF_KEY_OAUTH_TOKEN = "PREF_KEY_OAUTH_TOKEN";
    public static final String PREF_KEY_OAUTH_TOKEN_TYPE = "PREF_KEY_OAUTH_TOKEN_TYPE";
    public static final String PREF_KEY_USER_ID = "PREF_KEY_USER_ID";
    public static final String PREF_KEY_USER_NAME = "PREF_KEY_USER_NAME";
    public static final String PREF_KEY_USER_MOBILE = "PREF_KEY_USER_MOBILE";
    public static final String PREF_KEY_USER_COUNTRY_CODE = "PREF_KEY_USER_COUNTRY_CODE";
    public static final String PREF_KEY_USER_EMAIL = "PREF_KEY_USER_EMAIL";
    public static final String PREF_KEY_IS_LOGGED_IN = "PREF_KEY_IS_LOGGED_IN";
    public static final String PREF_KEY_IS_EMAIL_VALIDATE = "PREF_KEY_IS_EMAIL_VALIDATE";
    public static final String PREF_KEY_USER_IMG = "PREF_KEY_USER_IMG";


    public static String PREF_KEY_LANGUAGE = "PREF_KEY_LANGUAGE";
    public static String PREF_KEY_COUNTRY = "PREF_KEY_COUNTRY";

    public static final String KEY_PASSWORD = "KEY_PASSWORD";

    public static String appName = "IWANT_SMART_SHOP";
    public static String locale = "en";
    public static int nodeStartPageNo = 1;
    public static int nodePageMaxLimit = 100;
    public static int javaMaxSize = 1000;


    public static int RESPONSE_CODE = 200;
    public static String SERVER_NOT_READY = "Server not ready";

    public static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=";
    public static final String MARKET_PLACE_ID = "1";
    public static final String API_APP_NAME = "MARKETPLACE_CONSUMER_PENAL";
    public static final String LOCALE = "en";
    public static final String USER_DATE_FORMAT = "dd/MM/yyyy";
    public static final String REVIEW_DATE_FORMAT = "MMM dd, yyyy";
    public static final String TIME_FORMAT = "HH:mm";
    public static final Integer NODE_FIRST_PAGE_INDEX = 1;
    public static final Integer NODE_MAX_PAGE_LIMIT = -1;
    public static final Integer COLLECTION_ITEM_LIMIT = -1;
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String OFFER_TEXT = "OFFER_TEXT";
    public static final String TIMING = "TIMING";
    public static final String VALID_TILL = "VALID_TILL";
    public static final String MENU_PHOTO = "MENU_PHOTO";
    public static final String RES_NAME = "RES_NAME";
    public static final String RES_DETAIL = "RES_DETAIL";
    public static final String BOOKING_ID = "BOOKING_ID";


    // App COnfig
    public static final String PREF_KEY_CONTENT_BACK = "PREF_KEY_CONTENT_BACK";
    public static final String PREF_KEY_PLAYER_BACK = "PREF_KEY_PLAYER_BACK";

    public static final String CHANNELS_MOBILE = "MOBILE";
    public static final String FRAGMENT_NAME = "FRAGMENT_NAME";
    public static final String LANGUAGE = "LANGUAGE";

    public static final String KEY_USER = "KEY_USER";
    public static final String KEY_PWD = "KEY_PWD";
    public static final String KEY_OTP_ACTION = "KEY_OTP_ACTION";
    public static final String KEY_ACTIVITY_NAME = "KEY_ACTIVITY_NAME";
    public static final String FILTER_REQ= "FILTER_REQ";

    public static final String LANGUAGE_TEXT = "LANGUAGE_TEXT";

}
