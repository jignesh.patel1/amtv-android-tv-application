package amtv.iwant.amtvtv.core;

/**
 * Created by manish on 13/11/17.
 */

public class ApiErrorCode {

    public static String USER_NOT_FOUND = "IW-BUSRUSR005";
    public static String OTP_INVALID = "IW-BUSROTP001";
    public static String OTP_EXPIRED = "IW-BUSROTP002";
}
